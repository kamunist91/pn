﻿&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ПриОткрытииНаСервере();
КонецПроцедуры

&НаСервере
Процедура ПриОткрытииНаСервере()
	Автор = ИмяПользователя();
	Объект.Автор = Автор;
КонецПроцедуры
