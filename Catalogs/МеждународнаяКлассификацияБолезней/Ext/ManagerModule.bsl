﻿#Область Представление_Формы_оказания

Процедура ОбработкаПолученияПолейПредставления(Поля, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	Поля.Добавить("КодМКБ");
	Поля.Добавить("ПолноеНаименование");
КонецПроцедуры

Процедура ОбработкаПолученияПредставления(Данные, Представление, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	Представление = Данные.КодМКБ + " " + Данные.ПолноеНаименование;
КонецПроцедуры

#КонецОбласти