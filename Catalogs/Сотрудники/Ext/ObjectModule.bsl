﻿
Процедура ПередЗаписью(Отказ)
	ПоляДляФизЛица = ФизическиеЛицаСервер.ПолучитьСтруктуруФизЛица(ЭтотОбъект);
	ЭтотОбъект.ФизическоеЛицо = ФизическиеЛицаСервер.СоздатьОбновитьФизическоеЛицо(ЭтотОбъект.ФизическоеЛицо, ПоляДляФизЛица);
КонецПроцедуры
