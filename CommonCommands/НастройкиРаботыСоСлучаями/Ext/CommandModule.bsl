﻿
&НаКлиенте
Процедура ОбработкаКоманды(ПараметрКоманды, ПараметрыВыполненияКоманды)
	Настройки = ПолучитьНастройкиИзРегистра();
	Если Настройки = Неопределено Тогда
		ОткрытьФорму("РегистрСведений.НастройкиПользователей.Форма.ФормаЗаписи");
	Иначе
		ПараметрыФормы = Новый Структура("Ключ, ИзменятьПериод", ПолучитьКлючЗаписи(Настройки.Период,
										Настройки.Пользователь, Настройки.Подразделение,
										Настройки.Должность, Настройки.Специальность), Истина);
		ОткрытьФорму("РегистрСведений.НастройкиПользователей.Форма.ФормаЗаписи", ПараметрыФормы);
	КонецЕсли;
КонецПроцедуры

&НаСервере
Функция ПолучитьНастройкиИзРегистра()
	Настройки = НастройкиПользователяДляОформленияСлучаев.ПолучитьПользовательскиеНастройки(ТекущаяДата(),
				ПараметрыСеанса.ТекущийПользователь, ПараметрыСеанса.Подразделение, ПараметрыСеанса.Должность, ПараметрыСеанса.Специальность);
	Возврат Настройки;
КонецФункции

&НаСервере
Функция ПолучитьКлючЗаписи(Период, Пользователь, Подразделение, Должность, Специальность)
	Возврат РегистрыСведений.НастройкиПользователей.СоздатьКлючЗаписи(
			Новый Структура("Период,  Пользователь, Подразделение, Должность, Специальность",
			Период, Пользователь, Подразделение, Должность, Специальность));
КонецФункции