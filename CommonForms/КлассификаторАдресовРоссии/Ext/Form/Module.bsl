﻿
&НаКлиенте
Процедура КлассификаторАдресовРоссииНажатие(Элемент)
	ФормаСпискаКЛАДР = ПолучитьФорму("Справочник.КЛАДР.Форма.ФормаСписка");
	Если ФормаСпискаКЛАДР.Открыта() Тогда
		ФормаСпискаКЛАДР.Активизировать();
	Иначе
		ФормаСпискаКЛАДР.Открыть();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура УлицыРоссииНажатие(Элемент)
	ФормаСпискаУлиц = ПолучитьФорму("Справочник.Улицы.Форма.ФормаСписка");
	Если ФормаСпискаУлиц.Открыта() Тогда
		ФормаСпискаУлиц.Активизировать();
	Иначе
		ФормаСпискаУлиц.Открыть();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ВводАдресаПоКЛАДРНажатие(Элемент)
	ФормаВвода = ПолучитьФорму("Обработка.КЛАДР.Форма.ФормаВводаАдреса");
	Если ФормаВвода.Открыта() Тогда
		ФормаВвода.Активизировать();
	Иначе
		ФормаВвода.Открыть();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ОбновлениеКлассификатораАдресовРоссииНажатие(Элемент)
	ФормаОбновления = ПолучитьФорму("Обработка.ОбновлениеКЛАДР.Форма.ФормаОбновления");
	Если ФормаОбновления.Открыта() Тогда
		ФормаОбновления.Активизировать();
	Иначе
		ФормаОбновления.Открыть();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ОбщийКлассификаторСтранМираНажатие(Элемент)
	ФормаОКСМ = ПолучитьФорму("Справочник.ОКСМ.Форма.ФормаСписка");
	Если ФормаОКСМ.Открыта() Тогда
		ФормаОКСМ.Активизировать();
	Иначе
		ФормаОКСМ.Открыть();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ДатыАктуальностиКлассификатораАдресовРоссииНажатие(Элемент)
	ФормаСписка = ПолучитьФорму("РегистрСведений.АктуальностиКЛАДР.Форма.ФормаСписка");
	Если ФормаСписка.Открыта() Тогда
		ФормаСписка.Активизировать();
	Иначе
		ФормаСписка.Открыть();
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ВыборУлицыПоКЛАДРНажатие(Элемент)
	ФормаВвода = ПолучитьФорму("Обработка.КЛАДР.Форма.ФормаВыбораУлицы");
	Если ФормаВвода.Открыта() Тогда
		ФормаВвода.Активизировать();
	Иначе
		ФормаВвода.Открыть();
	КонецЕсли;
КонецПроцедуры
