﻿Процедура ЗагрузитьВакцины(ТемпФайлВакцинXLSX) Экспорт
	Попытка
		НачатьТранзакцию();
		сч = 0;
		Создано = 0;
		Обновлено = 0;
		
	    ИмяЛиста = Новый Структура ("ИмяЛиста, НомерЛиста", "Лист1", 1);
		СтрокаЗаголовка = 3;
		НачСтрока = 4;
		КонСтрока = 0;
		КолвоСтрокExcel = 0;
		ТаблицаXLSX = РаботаСEXCEL.ЗагрузитьМетодом_MSEXCEL(ТемпФайлВакцинXLSX,
					ИмяЛиста, СтрокаЗаголовка, НачСтрока, КонСтрока, КолвоСтрокExcel);
					
		Для чс = 1 По ТаблицаXLSX.Количество() - 1 Цикл
			Наименование = СокрЛП(ТаблицаXLSX[чс].N1);
			сч = сч + 1;
			Запрос = Новый Запрос;
			Запрос.Текст = 
				"ВЫБРАТЬ
				|	Вакцины.Ссылка,
				|	Вакцины.Наименование,
				|	Вакцины.Код
				|ИЗ
				|	Справочник.Вакцины КАК Вакцины
				|ГДЕ
				|	Вакцины.Наименование = &Наименование";
				
			Запрос.УстановитьПараметр("Наименование", Наименование);
			РезультатЗапроса = Запрос.Выполнить().Выбрать();
			Если РезультатЗапроса.Количество() = 1 Тогда
				РезультатЗапроса.Следующий();
				Вакцинка = РезультатЗапроса.Ссылка.ПолучитьОбъект();
				Обновлено = Обновлено + 1;
			Иначе
				Вакцинка = Справочники.Вакцины.СоздатьЭлемент();
				Создано = Создано + 1;
			КонецЕсли;
			Вакцинка.Наименование = ?(ТипЗнч(Наименование) = Тип("Строка"), Наименование, Неопределено);
			Вакцинка.Записать();
		КонецЦикла;
		ЗафиксироватьТранзакцию();
		Сообщить("Запись вакцин завершена!"
				+ Символы.ПС + "Записано - " + Строка(сч)
				+ Символы.ПС + "Из них обновлено - " + Строка(Обновлено) + ", "
				+ "создано новых записей - " + Строка(Создано));
	Исключение
		ОтменитьТранзакцию();
		Сообщить("Запись вакцин не удалась!" + Символы.ПС + ОписаниеОшибки());
	КонецПопытки;	
	
КонецПроцедуры
