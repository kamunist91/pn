﻿Функция ПолучитьСНИЛС() Экспорт
	Возврат Справочники.ВидыДокументов.НайтиПоНаименованию("СНИЛС");
КонецФункции

Функция ПолучитьПаспортРФ() Экспорт
	Возврат Справочники.ВидыДокументов.НайтиПоНаименованию("Паспорт гражданина Российской Федерации");
КонецФункции

Функция ПолучитьПаспортСССР() Экспорт
	Возврат Справочники.ВидыДокументов.НайтиПоНаименованию("Паспорт гражданина СССР");
КонецФункции

Функция ПолучитьСвидетельствоОРожденииРФ() Экспорт
	Возврат Справочники.ВидыДокументов.НайтиПоНаименованию("Свидетельство о рождении, выданное в Российской Федерации");
КонецФункции

Функция ПолучитьВоенныйБилет() Экспорт
	Возврат Справочники.ВидыДокументов.НайтиПоНаименованию("Военный билет");
КонецФункции

Функция ПолучитьЕдиныйПолисОМС() Экспорт
	Возврат Справочники.ВидыДокументов.НайтиПоНаименованию("Полис ОМС единого образца");
КонецФункции

Функция ПолучитьВременныйПолисОМС() Экспорт
	Возврат Справочники.ВидыДокументов.НайтиПоНаименованию("Временное свидетельство, подтверждающее оформление полиса обязательного медицинского страхования");
КонецФункции

Функция ПолучитьСтарыйПолисОМС() Экспорт
	Возврат Справочники.ВидыДокументов.НайтиПоНаименованию("Полис ОМС старого образца");
КонецФункции