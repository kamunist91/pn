﻿Процедура ПостановкаЗадачиВОчередьЕГИСЗПриЗаписи(Источник, Отказ) Экспорт
	Если ПациентыСервер.ПрикрепленВТФОМС(Источник.Ссылка)
		ИЛИ ПациентыСервер.ПроверкаПрикрепления(Источник.Ссылка) Тогда	
		Если ТипЗнч(Источник) = Тип("СправочникОбъект.Пациенты") Тогда
			МенеджерЗаписи = РегистрыСведений.ЗадачиПодсистемыЕГИСЗ.СоздатьМенеджерЗаписи();
			МенеджерЗаписи.Период = ТекущаяДата();
			МенеджерЗаписи.Объект = Источник.Ссылка;
			МенеджерЗаписи.Статус = Перечисления.СтатусыЗадачЕГИСЗ.Новая;
			МенеджерЗаписи.ИдентификаторЗадачи = Новый УникальныйИдентификатор;
			МенеджерЗаписи.Записать(Истина);
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

Процедура ОбработкаЗаданийЕГИСЗ(ВыполнитьОднуЗадачу = Ложь) Экспорт
	Если ТипЗнч(ВыполнитьОднуЗадачу) = Тип("Булево") И ВыполнитьОднуЗадачу = Истина Тогда
		Запрос = Новый Запрос("ВЫБРАТЬ ПЕРВЫЕ 1
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.Период КАК Период,
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.Объект,
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.Статус,
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.СтатусПослеВыгрузки,
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.ИдентификаторЗадачи,
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.Комментарий
		                      |ИЗ
		                      |	РегистрСведений.ЗадачиПодсистемыЕГИСЗ.СрезПоследних(&ТекущаяДата, ) КАК ЗадачиПодсистемыЕГИСЗСрезПоследних
		                      |ГДЕ
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыЗадачЕГИСЗ.Новая)
		                      |	И ЗадачиПодсистемыЕГИСЗСрезПоследних.СтатусПослеВыгрузки = ЗНАЧЕНИЕ(Перечисление.СтатусыЗадачЕГИСЗ.ПустаяСсылка)");
		Запрос.УстановитьПараметр("ТекущаяДата", ТекущаяДата());
		Выборка = Запрос.Выполнить().Выбрать();
	Иначе		
		Запрос = Новый Запрос("ВЫБРАТЬ
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.Период,
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.Объект,
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.Статус,
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.СтатусПослеВыгрузки,
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.ИдентификаторЗадачи,
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.Комментарий
		                      |ИЗ
		                      |	РегистрСведений.ЗадачиПодсистемыЕГИСЗ.СрезПоследних(&ТекущаяДата, ) КАК ЗадачиПодсистемыЕГИСЗСрезПоследних
		                      |ГДЕ
		                      |	ЗадачиПодсистемыЕГИСЗСрезПоследних.Статус = ЗНАЧЕНИЕ(Перечисление.СтатусыЗадачЕГИСЗ.Новая)
		                      |	И ЗадачиПодсистемыЕГИСЗСрезПоследних.СтатусПослеВыгрузки = ЗНАЧЕНИЕ(Перечисление.СтатусыЗадачЕГИСЗ.ПустаяСсылка)");
		Запрос.УстановитьПараметр("ТекущаяДата", ТекущаяДата());
		Выборка = Запрос.Выполнить().Выбрать();
	КонецЕсли;
	
	Пока Выборка.Следующий() Цикл
		//Если изменяются данные физ. лица
		Попытка
			ПараметрыОбработки = Новый Структура("Период, Объект, Статус, СтатусПослеВыгрузки, ИдентификаторЗадачи, Комментарий");
			ЗаполнитьЗначенияСвойств(ПараметрыОбработки, Выборка);
			Если ТипЗнч(ПараметрыОбработки.Объект) = Тип("СправочникСсылка.Пациенты") Тогда
				ВыполнитьОбработкуИзмененийДанныхФизЛица(ПараметрыОбработки);
				//ВызватьИсключение "Для данного класса объектов не задан обработчик";
			КонецЕсли;
			
			МенеджерЗаписи = РегистрыСведений.ЗадачиПодсистемыЕГИСЗ.СоздатьМенеджерЗаписи();
			ЗаполнитьЗначенияСвойств(МенеджерЗаписи, Выборка);
			МенеджерЗаписи.СтатусПослеВыгрузки = Перечисления.СтатусыЗадачЕГИСЗ.Выполнена;
			МенеджерЗаписи.Комментарий = "";
			МенеджерЗаписи.Записать(Истина);
			
		Исключение
			ИнформацияОбОшибке = ИнформацияОбОшибке();
			
			МенеджерЗаписи = РегистрыСведений.ЗадачиПодсистемыЕГИСЗ.СоздатьМенеджерЗаписи();
			ЗаполнитьЗначенияСвойств(МенеджерЗаписи, Выборка);
			МенеджерЗаписи.СтатусПослеВыгрузки = Перечисления.СтатусыЗадачЕГИСЗ.ОшибкаПриВыполнении;
			МенеджерЗаписи.Комментарий = ПодробноеПредставлениеОшибки(ИнформацияОбОшибке);
			МенеджерЗаписи.Записать(Истина);
		КонецПопытки;
	КонецЦикла;	
КонецПроцедуры

Процедура ВыполнитьОбработкуИзмененийДанныхФизЛица(СтрокаРегистра);
	КодПациентаВЕГИСЗ = ЕГИСЗПациенты.НайтиПациентаВЕГИСЗ(СтрокаРегистра.Объект);
	//Теперь необходимо последовательно получить ФИО, Документы и СНИЛС, и сравнить их с имеющимися, 
	//при необходимости обновить данные в ЕГИСЗ'е
	ОсновныеДанные = ЕГИСЗФизЛица.ПолучитьДанныеФизЛица(СтрокаРегистра.Объект);
	НовыеДанные = ПолучитьОсновныеДанныеПациента(СтрокаРегистра.Объект, СтрокаРегистра.Период);
	Если ИзмененыДанные(ОсновныеДанные, НовыеДанные, "Основные данные") Тогда
		ЕГИСЗФизЛица.ОбновлениеДанныхФизЛица(СтрокаРегистра.Объект, НовыеДанные);
	КонецЕсли;
	ОбработатьПрикреплениеПациента(КодПациентаВЕГИСЗ, СтрокаРегистра.Объект);
	ОбработатьАдресаФизЛица(СтрокаРегистра.Объект, СтрокаРегистра.Период);	
	ОбработатьДокументыФизЛица(СтрокаРегистра.Объект, СтрокаРегистра.Период);
КонецПроцедуры

#Область АдресаФизЛиц

Функция ОбработатьАдресаФизЛица(Пациент, Период)
	МассивПолныхОписанийАдресаФизЛица = ЕГИСЗФизЛица.ПолучитьПолноеОписаниеАдресовФизЛиц(Пациент);
	
	//Получим записи об адресах в локальной системе
	ЗапросАдресаВЛокальнойСистеме = Новый Запрос("ВЫБРАТЬ
	                                             |	ИсторияИзмененияПациентовСрезПоследних.АдресРегистрации КАК АдресРегистрации,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.КодРегионаРег КАК КодРегионаРег,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.КодРайонаРег КАК КодРайонаРег,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.КодГородаРег КАК КодГородаРег,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.КодНаселенногоПунктаРег КАК КодНаселенногоПунктаРег,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.КодУлицыРег КАК КодУлицыРег,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.ДомРег КАК ДомРег,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.КвартираРег КАК КвартираРег,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.ФактическийАдрес КАК ФактическийАдрес,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.КодРегионаФакт КАК КодРегионаФакт,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.КодРайонаФакт КАК КодРайонаФакт,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.КодГородаФакт КАК КодГородаФакт,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.КодНаселенногоПунктаФакт КАК КодНаселенногоПунктаФакт,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.КодУлицыФакт КАК КодУлицыФакт,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.ДомФакт КАК ДомФакт,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.КвартираФакт КАК КвартираФакт,
	                                             |	ИсторияИзмененияПациентовСрезПоследних.Период КАК Период
	                                             |ИЗ
	                                             |	РегистрСведений.ИсторияИзмененияПациентов.СрезПоследних(, Пациент = &Пациент) КАК ИсторияИзмененияПациентовСрезПоследних
	                                             |ГДЕ
	                                             |	ИсторияИзмененияПациентовСрезПоследних.Период В
	                                             |			(ВЫБРАТЬ
	                                             |				МАКСИМУМ(ИсторияИзмененияПациентовСрезПоследних1.Период) КАК Период
	                                             |			ИЗ
	                                             |				РегистрСведений.ИсторияИзмененияПациентов.СрезПоследних(, Пациент = &Пациент) КАК ИсторияИзмененияПациентовСрезПоследних1)");
	ЗапросАдресаВЛокальнойСистеме.Параметры.Вставить("Пациент", Пациент);
	
	АдресаВЛокальнойСистеме = ЗапросАдресаВЛокальнойСистеме.Выполнить().Выгрузить();
	Если АдресаВЛокальнойСистеме.Количество() = 0 Тогда
		Возврат Ложь;
	Иначе
		АдресаВЛокальнойСистеме = АдресаВЛокальнойСистеме[0];
	КонецЕсли;
	                     
	СовпадаетАдресРегистрации = Ложь;
	СовпадаетАдресФактический = Ложь;
	ПустойАдресРегистрации = Ложь;
	ПустойАдресФактический = Ложь;
	
	Если ПустаяСтрока(АдресаВЛокальнойСистеме.АдресРегистрации) Тогда
		ПустойАдресРегистрации = Истина;
	КонецЕсли;
	
	Если ПустаяСтрока(АдресаВЛокальнойСистеме.ФактическийАдрес) Тогда
		ПустойАдресФактический = Истина;
	КонецЕсли;
	
	//Обработаем адрес регистрации
	ТипАдресаРегистрации = Справочники.ВидыАдресов.НайтиПоКоду("000000001");
	Если ТипАдресаРегистрации.Пустая() Тогда
		ВызватьИсключение "Не найден тип адреса ""Адрес регистрации"" по коду 000000001";
	КонецЕсли;
	ИдентификаторТипаАдресаРегистрации = ЕГИСЗРаботаСоСправочниками.ПолучитьКодДополнительногоСвойства(ТипАдресаРегистрации, Справочники.ДополнительныеСвойстваЕГИСЗ.ВидАдреса);
	
	Если ПустойАдресРегистрации Тогда
		Для Каждого ПолноеОписаниеАдресаФизЛица из МассивПолныхОписанийАдресаФизЛица Цикл
			Если ПолноеОписаниеАдресаФизЛица.ТипыАдреса.Найти(ИдентификаторТипаАдресаРегистрации) <> Неопределено Тогда
				ПолноеОписаниеАдресаФизЛица.АдресСовпадает = Истина;
			КонецЕсли
		КонецЦикла;
	КонецЕсли;
	
	Для Каждого ПолноеОписаниеАдресаФизЛица из МассивПолныхОписанийАдресаФизЛица Цикл
		Если ПолноеОписаниеАдресаФизЛица.ТипыАдреса.Найти(ИдентификаторТипаАдресаРегистрации) <> Неопределено Тогда
			//Проверим, совпадает ли адрес
			//Для этого найдем в таблице адресов в удаленной системе строку с улицей,
			// и сравним ее код по КЛАДРУ
			СтрокиСУлицей = ПолноеОписаниеАдресаФизЛица.ПолноеОписаниеАдресаФизЛица.НайтиСтроки(Новый Структура("Уровень", "6"));
			Если СтрокиСУлицей.Количество() = 0 Тогда
				//По всей видимости это "кривой" адрес и его надо удалить
			Иначе
				СтрокаСУлицей = СтрокиСУлицей[0];
				Если СтрокаСУлицей.КодПоКЛАДР = АдресаВЛокальнойСистеме.КодУлицыРег Тогда
					СовпадаетАдресРегистрации = Истина;
					ПолноеОписаниеАдресаФизЛица.АдресСовпадает = Истина;
				КонецЕсли;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	//Обработаем адрес места жительства
	ТипАдресаМестаЖительства = Справочники.ВидыАдресов.НайтиПоКоду("000000002");
	Если ТипАдресаМестаЖительства.Пустая() Тогда
		ВызватьИсключение "Не найден тип адреса ""Адрес места жительства"" по коду 000000002";
	КонецЕсли;
	ИдентификаторТипаАдресаМестаЖительства = ЕГИСЗРаботаСоСправочниками.ПолучитьКодДополнительногоСвойства(ТипАдресаМестаЖительства, Справочники.ДополнительныеСвойстваЕГИСЗ.ВидАдреса);
	
	Если ПустойАдресФактический Тогда
		Для каждого ПолноеОписаниеАдресаФизЛица из МассивПолныхОписанийАдресаФизЛица Цикл
			Если ПолноеОписаниеАдресаФизЛица.ТипыАдреса.Найти(ИдентификаторТипаАдресаМестаЖительства) <> Неопределено Тогда
				СовпадаетАдресФактический = Истина;
				ПолноеОписаниеАдресаФизЛица.АдресСовпадает = Истина;
			КонецЕсли
		КонецЦикла;
	КонецЕсли;
	
	Для каждого ПолноеОписаниеАдресаФизЛица из МассивПолныхОписанийАдресаФизЛица Цикл
		Если ПолноеОписаниеАдресаФизЛица.ТипыАдреса.Найти(ИдентификаторТипаАдресаМестаЖительства) <> Неопределено Тогда
			//Проверим, совпадает ли адрес
			//Для этого найдем в таблице адресов в удаленной системе строку с улицей,
			// и сравним ее код по КЛАДРУ
			СтрокиСУлицей = ПолноеОписаниеАдресаФизЛица.ПолноеОписаниеАдресаФизЛица.НайтиСтроки(Новый Структура("Уровень", "6"));
			Если СтрокиСУлицей.Количество() = 0 Тогда
				//По всей видимости это "кривой" адрес и его надо удалить
			Иначе
				СтрокаСУлицей = СтрокиСУлицей[0];
				Если СтрокаСУлицей.КодПоКЛАДР = АдресаВЛокальнойСистеме.КодУлицыРег Тогда
					СовпадаетАдресФактический = Истина;
					ПолноеОписаниеАдресаФизЛица.АдресСовпадает = Истина;
				КонецЕсли;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	//Удалим неидентифицированные адреса
	Для каждого ПолноеОписаниеАдресаФизЛица из МассивПолныхОписанийАдресаФизЛица Цикл
		Если не ПолноеОписаниеАдресаФизЛица.АдресСовпадает Тогда
			РезультатУдаления = ЕГИСЗФизЛица.УдалитьЗаписьОбАдресе(ПолноеОписаниеАдресаФизЛица.ИдентификаторЗаписиАдреса);
			Если РезультатУдаления <> ПолноеОписаниеАдресаФизЛица.ИдентификаторЗаписиАдреса Тогда
				//По к-л причине не удалось удалить адрес в удаленной системе
				Продолжить;
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
	
	//Добавим необходимые адреса
	Если не СовпадаетАдресРегистрации и не ПустойАдресРегистрации Тогда
		ИдентификаторЗаписи = ЕГИСЗФизЛица.ДобавитьЗаписьОбАдресе(Пациент, АдресаВЛокальнойСистеме.КодУлицыРег, АдресаВЛокальнойСистеме.ДомРег, АдресаВЛокальнойСистеме.КвартираРег, АдресаВЛокальнойСистеме.Период, Истина);
		ЕГИСЗФизЛица.ПривязатьТипАдреса(ТипАдресаРегистрации, ИдентификаторЗаписи);
	КонецЕсли;
	
	Если не СовпадаетАдресФактический и не ПустойАдресФактический Тогда
		ИдентификаторЗаписи = ЕГИСЗФизЛица.ДобавитьЗаписьОбАдресе(Пациент, АдресаВЛокальнойСистеме.КодУлицыФакт, АдресаВЛокальнойСистеме.ДомФакт, АдресаВЛокальнойСистеме.КвартираФакт, АдресаВЛокальнойСистеме.Период, Ложь);
		ЕГИСЗФизЛица.ПривязатьТипАдреса(ТипАдресаМестаЖительства, ИдентификаторЗаписи);
	КонецЕсли;
КонецФункции

// Удаляет а после записывает заново адреса в ЕГИСЗ
Функция ПереписатьАдреса(Пациент, Период)
	//
КонецФункции

// Получает документы пациента в локальной системе
//
// Параметры:
// Пациент - СправочникСсылка.Пациенты - Ссылка на пациента
// Дата - Дата - Дата, на которую надо получить документы пациента
Функция ПолучитьАдресаПациентаВСистеме(Пациент, Дата)
	Запрос = Новый Запрос("ВЫБРАТЬ
	                      |	ИсторияИзмененияПациентовСрезПоследних.Пациент,
	                      |	ИсторияИзмененияПациентовСрезПоследних.АдресРегистрации КАК ПредставлениеАдреса,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КодРегионаРег КАК КодРегиона,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КодРайонаРег КАК КодРайона,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КодГородаРег КАК КодГорода,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КодНаселенногоПунктаРег КАК КодНаселенногоПункта,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КодУлицыРег КАК КодУлицы,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ДомРег КАК Дом,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КвартираРег КАК Квартира,
	                      |	""Регистрации"" КАК ВидАдреса
	                      |ИЗ
	                      |	РегистрСведений.ИсторияИзмененияПациентов.СрезПоследних(&ДатаОтчета, Пациент = &Пациент) КАК ИсторияИзмененияПациентовСрезПоследних
	                      |
	                      |ОБЪЕДИНИТЬ ВСЕ
	                      |
	                      |ВЫБРАТЬ
	                      |	ИсторияИзмененияПациентовСрезПоследних.Пациент,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ФактическийАдрес,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КодРегионаФакт,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КодРайонаФакт,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КодГородаФакт,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КодНаселенногоПунктаФакт,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КодУлицыФакт,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ДомФакт,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КвартираФакт,
	                      |	""Фактический""
	                      |ИЗ
	                      |	РегистрСведений.ИсторияИзмененияПациентов.СрезПоследних(&ДатаОтчета, Пациент = &Пациент) КАК ИсторияИзмененияПациентовСрезПоследних");
	Запрос.Параметры.Вставить("ДатаОтчета", Дата);
	Запрос.Параметры.Вставить("Пациент", Пациент);
	Возврат Запрос.Выполнить().Выгрузить();
КонецФункции

#КонецОбласти

#Область ДокументыФизЛиц

// Обрабатывает документы пациента в локальной системе, 
// сравнивает их с документами ЕГИСЗа и при необходимости
// обновляет/добавляет
//
// Параметры:
// Пациент - СправочникСсылка.Пациенты - Ссылка на пациента
// Дата - Дата - Дата, на которую надо получить документы пациента
// ДокументыФизЛицаВЕГИСЗ - ТаблицаЗначений - документы пациента в системе ЕГИСЗ, 
// можно получить по функции ЕГИСЗФизЛица.ПолучитьДокументыФизЛица
Функция ОбработатьДокументыФизЛица(Пациент, Период)
	//Нам необходимо проверить, есть ли в документах физлица актуальные данные по СНИЛС'у и полису
	ДокументыФизЛицаВЕГИСЗ = ЕГИСЗФизЛица.ПолучитьДокументыФизЛица(Пациент); //Возвращает ТЗ
	ТекущиеДокументыФизЛица = ПолучитьДокументыПациентаВСистеме(Пациент, Период); //Возвращает ТЗ (Добавил СМО)
	
	Для Каждого СтрокаТекущихДокументов Из ТекущиеДокументыФизЛица Цикл
		ДокументНайден = Ложь;
		ДокументНайденЧастично = Ложь;
		
		СтруктураПоиска = Новый Структура;
		СтруктураПоиска.Вставить("ВидДокумента", ЕГИСЗРаботаСоСправочниками.ПолучитьКодДополнительногоСвойства(
									СтрокаТекущихДокументов.ВидДокумента, Справочники.ДополнительныеСвойстваЕГИСЗ.ВидДокумента
									)
								);
		ИскомыеДокументыВЕГИСЗ = ДокументыФизЛицаВЕГИСЗ.НайтиСтроки(СтруктураПоиска);
		Для Каждого СтрокаДокументовФизЛица Из ИскомыеДокументыВЕГИСЗ Цикл
			Если Не ЗначениеЗаполнено(СтрокаТекущихДокументов.ВидДокумента) Тогда
				Продолжить;
			КонецЕсли;
			Если СтрокаТекущихДокументов.ВидДокумента = ВидыДокументовСервер.ПолучитьСНИЛС() Тогда
				Если УдалитьНенужныеСимволыИзНомераДокумента(СтрокаТекущихДокументов.НомерДокумента) = УдалитьНенужныеСимволыИзНомераДокумента(СтрокаДокументовФизЛица.НомерДокумента) Тогда
					ДокументНайден = Истина;
				КонецЕсли;
			Иначе
				Если УдалитьНенужныеСимволыИзНомераДокумента(СтрокаТекущихДокументов.НомерДокумента) = УдалитьНенужныеСимволыИзНомераДокумента(СтрокаДокументовФизЛица.НомерДокумента) 
					И УдалитьНенужныеСимволыИзНомераДокумента(СтрокаТекущихДокументов.СерияДокумента) = УдалитьНенужныеСимволыИзНомераДокумента(СтрокаДокументовФизЛица.СерияДокумента) Тогда
					ДокументНайденЧастично = Истина;
				КонецЕсли;
				Если Не СтрокаТекущихДокументов.ВидДокумента.ПередаватьПолныеДанные Тогда
					Если ?(СтрокаТекущихДокументов.ДатаВыдачи <> Дата(1,1,1), СтрокаТекущихДокументов.ДатаВыдачи, Неопределено) = СтрокаДокументовФизЛица.ДатаВыдачи
						и СтрокаТекущихДокументов.КтоВыдал = СтрокаДокументовФизЛица.КтоВыдал
						и ?(СтрокаТекущихДокументов.ДатаОкончанияДействия <> Дата(1,1,1), СтрокаТекущихДокументов.ДатаОкончанияДействия, Неопределено) = СтрокаДокументовФизЛица.ДатаОкончанияДействия Тогда
						ДокументНайден = Истина;
					КонецЕсли;
				Иначе
					Если СтрокаТекущихДокументов.ДатаВыдачи = СтрокаДокументовФизЛица.ДатаВыдачи
						и СтрокаТекущихДокументов.КтоВыдал = СтрокаДокументовФизЛица.КтоВыдал
						и ?(СтрокаТекущихДокументов.ДатаОкончанияДействия <> Дата(1,1,1), СтрокаТекущихДокументов.ДатаОкончанияДействия, Неопределено) = СтрокаДокументовФизЛица.ДатаОкончанияДействия
						и СтрокаТекущихДокументов.КтоВыдал = СтрокаДокументовФизЛица.КтоВыдал
						и СтрокаТекущихДокументов.Фамилия = СтрокаДокументовФизЛица.Фамилия
						и СтрокаТекущихДокументов.Имя = СтрокаДокументовФизЛица.Имя
						и СтрокаТекущихДокументов.Отчество = СтрокаДокументовФизЛица.Отчество
						и СтрокаТекущихДокументов.ДатаРождения = СтрокаДокументовФизЛица.ДатаРождения 
						и УдалитьНенужныеСимволыИзНомераДокумента(СтрокаТекущихДокументов.НомерДокумента) = УдалитьНенужныеСимволыИзНомераДокумента(СтрокаДокументовФизЛица.НомерДокумента) 
						и УдалитьНенужныеСимволыИзНомераДокумента(СтрокаТекущихДокументов.СерияДокумента) = УдалитьНенужныеСимволыИзНомераДокумента(СтрокаДокументовФизЛица.СерияДокумента) Тогда
						ДокументНайден = Истина;
					КонецЕсли;
				КонецЕсли;
			КонецЕсли;
			Если ДокументНайденЧастично И Не ДокументНайден Тогда
				Актуальность = ПолучитьАктуальностьДокумента(Пациент, СтрокаТекущихДокументов.ВидДокумента, СтрокаТекущихДокументов.НомерДокумента, Период);
				ЕГИСЗФизЛица.ОбновитьДокументФизЛица(СтрокаДокументовФизЛица.ИД, Пациент, СтрокаТекущихДокументов, Актуальность);
				Если Актуальность Тогда
					ОбновитьАктуальностьДокументов(Пациент, СтрокаТекущихДокументов);
				КонецЕсли;
			КонецЕсли;
		КонецЦикла;
		Если ДокументНайден Тогда
			//Актуальность = ПолучитьАктуальностьДокумента(Пациент, СтрокаТекущихДокументов.ВидДокумента, СтрокаТекущихДокументов.НомерДокумента, Период);
			//ЕГИСЗФизЛица.ОбновитьДокументФизЛица(СтрокаДокументовФизЛица.ИД, Пациент, СтрокаТекущихДокументов, Актуальность);
			//Если Актуальность Тогда
			//	ОбновитьАктуальностьДокументов(Пациент, СтрокаТекущихДокументов);
			//КонецЕсли;
		Иначе
			Актуальность = ПолучитьАктуальностьДокумента(Пациент, СтрокаТекущихДокументов.ВидДокумента, СтрокаТекущихДокументов.НомерДокумента, Период);
			ЕГИСЗФизЛица.СоздатьДокументДляФизЛица(Пациент, СтрокаТекущихДокументов, Актуальность);
			Если Актуальность Тогда
				ОбновитьАктуальностьДокументов(Пациент, СтрокаТекущихДокументов);
			КонецЕсли;
		КонецЕсли;
	КонецЦикла;
КонецФункции

Функция ОбновитьАктуальностьДокументов(Пациент, СтрокаТекущихДокументов)
	ДокументыФизЛицаВЕГИСЗ = ЕГИСЗФизЛица.ПолучитьДокументыФизЛица(Пациент);		
	Для Каждого СтрокаДокументовДляСнятияАктивации из ДокументыФизЛицаВЕГИСЗ Цикл
		Если СтрокаДокументовДляСнятияАктивации.ВидДокумента = ЕГИСЗРаботаСоСправочниками.ПолучитьКодДополнительногоСвойства(СтрокаТекущихДокументов.ВидДокумента, Справочники.ДополнительныеСвойстваЕГИСЗ.ВидДокумента)
			И (СтрокаДокументовДляСнятияАктивации.СерияДокумента <> СтрокаТекущихДокументов.СерияДокумента
			ИЛИ СтрокаДокументовДляСнятияАктивации.НомерДокумента <> СтрокаТекущихДокументов.НомерДокумента) Тогда
			ЕГИСЗФизЛица.ОбновитьДокументФизЛица(СтрокаДокументовДляСнятияАктивации.ИД, Пациент, СтрокаДокументовДляСнятияАктивации, Ложь);
		КонецЕсли
	КонецЦикла;
КонецФункции

// Возвращает Истина в том случае, если нет записей по пациенту и ВидуДокумента
// после заданной даты
Функция ПолучитьАктуальностьДокумента(ФизЛицо, ВидДокумента, НомерДокумента, Период)
	Запрос = Новый Запрос("ВЫБРАТЬ
	                      |	ИсторияИзмененияПациентов.Период
	                      |ИЗ
	                      |	РегистрСведений.ИсторияИзмененияПациентов КАК ИсторияИзмененияПациентов
	                      |ГДЕ
	                      |	ИсторияИзмененияПациентов.Период > &Период
	                      |	И ИсторияИзмененияПациентов.ВидДокументаУДЛ = &ВидДокументаУДЛ
	                      |	И ИсторияИзмененияПациентов.Пациент = &Пациент
	                      |	И ИсторияИзмененияПациентов.НомерУДЛ <> &НомерУДЛ");
	Запрос.Параметры.Вставить("Пациент", ФизЛИцо);
	Запрос.Параметры.Вставить("Период", Период);
	Запрос.Параметры.Вставить("НомерУДЛ", НомерДокумента);
	Запрос.Параметры.Вставить("ВидДокументаУДЛ", ВидДокумента);
	Выборка = Запрос.Выполнить().Выбрать();
	Если Не Выборка.Следующий() Тогда
		Возврат Истина;
	Иначе
		Возврат Ложь;
	КонецЕсли;
КонецФункции

// Получает документы пациента в локальной системе
//
// Параметры:
// Пациент - СправочникСсылка.Пациенты - Ссылка на пациента
// Дата - Дата - Дата, на которую надо получить документы пациента
Функция ПолучитьДокументыПациентаВСистеме(Пациент, ДатаОтчета)
	Запрос = Новый Запрос("ВЫБРАТЬ
	                      |	ИсторияИзмененияПациентовСрезПоследних.Пациент,
	                      |	ИсторияИзмененияПациентовСрезПоследних.СНИЛС КАК НомерДокумента,
	                      |	&ДокументСНИЛС КАК ВидДокумента,
	                      |	"""" КАК СерияДокумента,
	                      |	"""" КАК СМО,
						  |	"""" КАК КтоВыдал,	                      
	                      |	НЕОПРЕДЕЛЕНО КАК ДатаВыдачи,
	                      |	НЕОПРЕДЕЛЕНО КАК ДатаОкончанияДействия,
	                      |	"""" КАК КодПодразделения,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Фамилия,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Имя,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Отчество,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ДатаРождения,
	                      |	ЛОЖЬ КАК ЭтоПолис,
	                      |	ЛОЖЬ КАК ЭтоПаспорт
	                      |ИЗ
	                      |	РегистрСведений.ИсторияИзмененияПациентов.СрезПоследних(&ДатаОтчета, Пациент = &Пациент) КАК ИсторияИзмененияПациентовСрезПоследних
	                      |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ (ВЫБРАТЬ
	                      |			ИсторияИзмененияПациентовСрезПоследних.Пациент КАК Пациент,
	                      |			МАКСИМУМ(ИсторияИзмененияПациентовСрезПоследних.Период) КАК Период
	                      |		ИЗ
	                      |			РегистрСведений.ИсторияИзмененияПациентов.СрезПоследних(&ДатаОтчета, Пациент = &Пациент) КАК ИсторияИзмененияПациентовСрезПоследних
	                      |		ГДЕ
	                      |			ИсторияИзмененияПациентовСрезПоследних.СНИЛС <> """"
	                      |			И ИсторияИзмененияПациентовСрезПоследних.Пациент = &Пациент
	                      |		
	                      |		СГРУППИРОВАТЬ ПО
	                      |			ИсторияИзмененияПациентовСрезПоследних.Пациент) КАК ВложенныйЗапрос
	                      |		ПО ИсторияИзмененияПациентовСрезПоследних.Период = ВложенныйЗапрос.Период
	                      |			И ИсторияИзмененияПациентовСрезПоследних.Пациент = ВложенныйЗапрос.Пациент
	                      |ГДЕ
	                      |	ИсторияИзмененияПациентовСрезПоследних.СНИЛС <> """"
	                      |
	                      |СГРУППИРОВАТЬ ПО
	                      |	ИсторияИзмененияПациентовСрезПоследних.Пациент,
	                      |	ИсторияИзмененияПациентовСрезПоследних.СНИЛС,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Фамилия,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Имя,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Отчество,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ДатаРождения
	                      |
	                      |ОБЪЕДИНИТЬ ВСЕ
	                      |
	                      |ВЫБРАТЬ
	                      |	ИсторияИзмененияПациентовСрезПоследних.Пациент,
	                      |	ИсторияИзмененияПациентовСрезПоследних.НомерПолиса,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ВидПолиса,
	                      |	ИсторияИзмененияПациентовСрезПоследних.СерияПолиса,
	                      |	ИсторияИзмененияПациентовСрезПоследних.СМО,
						  |	ИсторияИзмененияПациентовСрезПоследних.СМО.Наименование,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ДатаВыдачиПолиса,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ДатаОкончанияДействияПолиса,
	                      |	"""",
	                      |	ИсторияИзмененияПациентовСрезПоследних.Фамилия,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Имя,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Отчество,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ДатаРождения,
	                      |	ИСТИНА,
	                      |	ЛОЖЬ
	                      |ИЗ
	                      |	РегистрСведений.ИсторияИзмененияПациентов.СрезПоследних(&ДатаОтчета, Пациент = &Пациент) КАК ИсторияИзмененияПациентовСрезПоследних
	                      |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ (ВЫБРАТЬ
	                      |			ИсторияИзмененияПациентовСрезПоследних.Пациент КАК Пациент,
	                      |			МАКСИМУМ(ИсторияИзмененияПациентовСрезПоследних.Период) КАК Период
	                      |		ИЗ
	                      |			РегистрСведений.ИсторияИзмененияПациентов.СрезПоследних(&ДатаОтчета, Пациент = &Пациент) КАК ИсторияИзмененияПациентовСрезПоследних
	                      |		ГДЕ
	                      |			ИсторияИзмененияПациентовСрезПоследних.Пациент = &Пациент
	                      |		
	                      |		СГРУППИРОВАТЬ ПО
	                      |			ИсторияИзмененияПациентовСрезПоследних.Пациент) КАК ВложенныйЗапрос
	                      |		ПО ИсторияИзмененияПациентовСрезПоследних.Период = ВложенныйЗапрос.Период
	                      |			И ИсторияИзмененияПациентовСрезПоследних.Пациент = ВложенныйЗапрос.Пациент
	                      |
	                      |ОБЪЕДИНИТЬ ВСЕ
	                      |
	                      |ВЫБРАТЬ
	                      |	ИсторияИзмененияПациентовСрезПоследних.Пациент,
	                      |	ИсторияИзмененияПациентовСрезПоследних.НомерУДЛ,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ВидДокументаУДЛ,
	                      |	ИсторияИзмененияПациентовСрезПоследних.СерияУДЛ,
	                      |	"""",
						  |	ИсторияИзмененияПациентовСрезПоследних.КемВыданУДЛ,	                      
	                      |	ИсторияИзмененияПациентовСрезПоследних.ДатаВыдачиУДЛ,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ДатаОкончанияДействияУДЛ,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КодПодразделенияУДЛ,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Фамилия,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Имя,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Отчество,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ДатаРождения,
	                      |	ЛОЖЬ,
	                      |	ИСТИНА
	                      |ИЗ
	                      |	РегистрСведений.ИсторияИзмененияПациентов.СрезПоследних(&ДатаОтчета, Пациент = &Пациент) КАК ИсторияИзмененияПациентовСрезПоследних
	                      |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ (ВЫБРАТЬ
	                      |			ИсторияИзмененияПациентовСрезПоследних.Пациент КАК Пациент,
	                      |			МАКСИМУМ(ИсторияИзмененияПациентовСрезПоследних.Период) КАК Период
	                      |		ИЗ
	                      |			РегистрСведений.ИсторияИзмененияПациентов.СрезПоследних(&ДатаОтчета, Пациент = &Пациент) КАК ИсторияИзмененияПациентовСрезПоследних
	                      |		ГДЕ
	                      |			ИсторияИзмененияПациентовСрезПоследних.Пациент = &Пациент
	                      |		
	                      |		СГРУППИРОВАТЬ ПО
	                      |			ИсторияИзмененияПациентовСрезПоследних.Пациент) КАК ВложенныйЗапрос
	                      |		ПО ИсторияИзмененияПациентовСрезПоследних.Период = ВложенныйЗапрос.Период
	                      |			И ИсторияИзмененияПациентовСрезПоследних.Пациент = ВложенныйЗапрос.Пациент
	                      |
	                      |СГРУППИРОВАТЬ ПО
	                      |	ИсторияИзмененияПациентовСрезПоследних.Пациент,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ВидДокументаУДЛ,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КемВыданУДЛ,
	                      |	ИсторияИзмененияПациентовСрезПоследних.КодПодразделенияУДЛ,
	                      |	ИсторияИзмененияПациентовСрезПоследних.СерияУДЛ,
	                      |	ИсторияИзмененияПациентовСрезПоследних.НомерУДЛ,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ДатаВыдачиУДЛ,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ДатаОкончанияДействияУДЛ,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Фамилия,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Имя,
	                      |	ИсторияИзмененияПациентовСрезПоследних.Отчество,
	                      |	ИсторияИзмененияПациентовСрезПоследних.ДатаРождения");
	Запрос.Параметры.Вставить("ДатаОтчета", ДатаОтчета);
	Запрос.Параметры.Вставить("ДокументСНИЛС", ВидыДокументовСервер.ПолучитьСНИЛС());
	Запрос.Параметры.Вставить("Пациент", Пациент);
	Результат = Запрос.Выполнить().Выгрузить();
	
	мУдалить = Новый Массив;
	Для каждого СтрокаДокументов Из Результат Цикл
		Если Не ЗначениеЗаполнено(СтрокаДокументов.ВидДокумента) Тогда
			мУдалить.Добавить(СтрокаДокументов);
		КонецЕсли;
	КонецЦикла;
	
	Для Каждого УдаляемаяСтрока Из мУдалить Цикл
		Результат.Удалить(УдаляемаяСтрока);
	КонецЦикла;
	
	Возврат Результат;
КонецФункции

#КонецОбласти

#Область ПрикреплениеПациентов

Функция ОбработатьПрикреплениеПациента(КодПациентаВЕГИСЗ, Пациент)
	Прикреплен = Ложь;
	ЕстьЗаписи = Ложь;
	
	Если ПациентыСервер.ПрикрепленВТФОМС(Пациент)
		ИЛИ ПациентыСервер.ПроверкаПрикрепления(Пациент) Тогда
		Прикреплен = Истина;
	КонецЕсли;
	ПрикрепленияПациентаВЕГИСЗ = ЕГИСЗПациенты.ПолучитьПрикрепленияПациента(КодПациентаВЕГИСЗ);
	Если ПрикрепленияПациентаВЕГИСЗ.Количество() > 0 Тогда
		ЕстьЗаписи = Истина;
		ПрикрепленияПирогова = НайтиПрикрепленияПирогова(ПрикрепленияПациентаВЕГИСЗ);
	КонецЕсли;		
	Если Прикреплен Тогда
		Если ЕстьЗаписи Тогда
			Если ПрикрепленияПирогова.Количество() = 0 Тогда
				СозданноеПрикрепление = ЕГИСЗПациенты.СоздатьПрикреплениеПациента(КодПациентаВЕГИСЗ, Пациент);
			Иначе
				Для Каждого ПрикПирогова Из ПрикрепленияПирогова Цикл
					УдаленноеПрикрепление = ЕГИСЗПациенты.УдалитьПрикреплениеПациента(ПрикПирогова);
				КонецЦикла;
				СозданноеПрикрепление = ЕГИСЗПациенты.СоздатьПрикреплениеПациента(КодПациентаВЕГИСЗ, Пациент);
			КонецЕсли;
		Иначе
			СозданноеПрикрепление = ЕГИСЗПациенты.СоздатьПрикреплениеПациента(КодПациентаВЕГИСЗ, Пациент);
		КонецЕсли;
	Иначе
		Если ЕстьЗаписи Тогда
			Если ПрикрепленияПирогова.Количество() > 0 Тогда
				Для Каждого ПрикПирогова Из ПрикрепленияПирогова Цикл
					УдаленноеПрикрепление = ЕГИСЗПациенты.УдалитьПрикреплениеПациента(ПрикПирогова);
				КонецЦикла;
			КонецЕсли;
		КонецЕсли;
	КонецЕсли;
КонецФункции

Функция НайтиПрикрепленияПирогова(ПрикрепленияПациентаВЕГИСЗ)
	МО = ЕГИСЗРаботаСоСправочниками.ПолучитьКодДополнительногоСвойства(Справочники.Организации.НайтиПоКоду("000000001"), Справочники.ДополнительныеСвойстваЕГИСЗ.Организация);
	КодыПрикреплений = Новый Массив;
	Для Каждого Прикрепление Из ПрикрепленияПациентаВЕГИСЗ Цикл
		Если Прикрепление.МедицинскаяОрганизация = МО Тогда
			КодыПрикреплений.Добавить(Прикрепление.КодПрикрепленияВЕГИСЗ);
		КонецЕсли;
	КонецЦикла;
	Возврат КодыПрикреплений;
КонецФункции

#КонецОбласти

#Область ВспомогательныеФункции

Функция УдалитьНенужныеСимволыИзНомераДокумента(Знач НомерДокумента)
	Строка = НомерДокумента;
	Для каждого НенужныйСимвол из ПолучитьСписокНенужныхСимволовДляНомеровДокументов() Цикл
		Строка = СтрЗаменить(Строка, НенужныйСимвол, "");
	КонецЦикла;
	Возврат Строка;
КонецФункции

Функция ПолучитьСписокНенужныхСимволовДляНомеровДокументов()
	Результат = Новый Массив;
	Результат.Добавить("-");
	Результат.Добавить(" ");
	Возврат Результат;
КонецФункции

// Получает ФИО, дату рождения и пол пациента на заданную дату
Функция ПолучитьОсновныеДанныеПациента(Пациент, ДатаОтчета)
	Запрос = Новый Запрос("ВЫБРАТЬ
	                      |	ИсторияИзмененияПациентов.Пациент,
	                      |	ИсторияИзмененияПациентов.Фамилия,
	                      |	ИсторияИзмененияПациентов.Имя,
	                      |	ИсторияИзмененияПациентов.Отчество,
	                      |	ИсторияИзмененияПациентов.ДатаРождения,
	                      |	ИсторияИзмененияПациентов.Пол,
	                      |	ИсторияИзмененияПациентов.ДатаСмерти
	                      |ИЗ
	                      |	РегистрСведений.ИсторияИзмененияПациентов КАК ИсторияИзмененияПациентов
	                      |		ВНУТРЕННЕЕ СОЕДИНЕНИЕ (ВЫБРАТЬ
	                      |			ИсторияИзмененияПациентов.Пациент КАК Пациент,
	                      |			МАКСИМУМ(ИсторияИзмененияПациентов.Период) КАК Период
	                      |		ИЗ
	                      |			РегистрСведений.ИсторияИзмененияПациентов КАК ИсторияИзмененияПациентов
	                      |		ГДЕ
	                      |			ИсторияИзмененияПациентов.Пациент = &Пациент
	                      |			И ИсторияИзмененияПациентов.Период <= &ДатаОтчета
	                      |		
	                      |		СГРУППИРОВАТЬ ПО
	                      |			ИсторияИзмененияПациентов.Пациент) КАК ВложенныйЗапрос
	                      |		ПО ИсторияИзмененияПациентов.Период = ВложенныйЗапрос.Период
	                      |			И ИсторияИзмененияПациентов.Пациент = ВложенныйЗапрос.Пациент
	                      |ГДЕ
	                      |	ИсторияИзмененияПациентов.Пациент = &Пациент
	                      |
	                      |СГРУППИРОВАТЬ ПО
	                      |	ИсторияИзмененияПациентов.Имя,
	                      |	ИсторияИзмененияПациентов.Отчество,
	                      |	ИсторияИзмененияПациентов.ДатаРождения,
	                      |	ИсторияИзмененияПациентов.Пол,
	                      |	ИсторияИзмененияПациентов.Пациент,
	                      |	ИсторияИзмененияПациентов.Фамилия,
	                      |	ИсторияИзмененияПациентов.ДатаСмерти");
	Запрос.Параметры.Вставить("ДатаОтчета", ДатаОтчета);
	Запрос.Параметры.Вставить("Пациент", Пациент);
	Выборка = Запрос.Выполнить().Выбрать();
	Если Выборка.Следующий() Тогда
		Возврат Выборка;
	Иначе
		Возврат Неопределено
	КонецЕсли;
КонецФункции

// Возвращает Истина, если переданные наборы полей отличаются друг от друга
// набор сравниваемых полей зависит от типа СравниваемогоОбъект'а и 
// вычисляется функцией ПолучитьСписокСравниваемыхПолей
Функция ИзмененыДанные(ОсновныеДанные, НовыеДанные, СравниваемыйОбъект)
	Для каждого ПолеСравнения из ПолучитьСписокСравниваемыхПолей(СравниваемыйОбъект) Цикл
		Если ОсновныеДанные[ПолеСравнения.Значение] <> НовыеДанные[ПолеСравнения.Представление] Тогда
			Возврат Истина;
		КонецЕсли;
	КонецЦикла;
	Возврат Ложь;
КонецФункции

// Возвращает набор сравниваемых полей, зависит от типа СравниваемогоОбъект'а и 
Функция ПолучитьСписокСравниваемыхПолей(СравниваемыйОбъект)
	Результат = Новый СписокЗначений;
	Если СравниваемыйОбъект = "Основные данные" Тогда
		Результат.Добавить("surname", "Фамилия");
		Результат.Добавить("Name", "Имя");
		Результат.Добавить("patrName", "Отчество");
		Результат.Добавить("birthDate", "ДатаРождения");
	КонецЕсли;
	Возврат Результат;
КонецФункции

#КонецОбласти