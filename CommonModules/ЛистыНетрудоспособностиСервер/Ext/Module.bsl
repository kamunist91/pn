﻿Функция ПолучитьПричинуНетрудоспособностиПоКоду(КодПричины) Экспорт
	Возврат Справочники.ЛНПричиныНетрудоспособности.НайтиПоРеквизиту("КодПричиныНетрудоспособности", КодПричины);
КонецФункции

Функция ПолучитьДополнительнуюПричинуНетрудоспособностиПоКоду(КодПричины) Экспорт
	Возврат Справочники.ЛНДополнительныеПричиныНетрудоспособности.НайтиПоРеквизиту("КодДополнительнойПричиныНетрудоспособности", КодПричины);
КонецФункции

Функция ПолучитьИнуюПричинуЗакрытияПоКоду(КодПричины) Экспорт
	Возврат Справочники.ЛНИныеПричиныЗакрытия.НайтиПоРеквизиту("КодИнойПричиныЗакрытия", КодПричины);
КонецФункции

Функция ПолучитьНарушениеРежимаПоКоду(КодНарушенияРежима) Экспорт
	Возврат Справочники.ЛННарушенияРежима.НайтиПоРеквизиту("КодНарушенияРежима", КодНарушенияРежима);
КонецФункции

Функция ПолучитьЗастрахованногоЛН(СсылкаНаЛН) Экспорт
	Возврат СсылкаНаЛН.Застрахованный;
КонецФункции

Функция ПолучитьВсеЛНПоПациенту(СсылкаНаПациента) Экспорт
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ЛистНетрудоспособности.Ссылка
		|ИЗ
		|	Документ.ЛистНетрудоспособности КАК ЛистНетрудоспособности
		|ГДЕ
		|	ЛистНетрудоспособности.Пациент = &СсылкаНаПациента";
	Запрос.УстановитьПараметр("СсылкаНаПациента", СсылкаНаПациента);	
	РезультатЗапроса = Запрос.Выполнить().Выгрузить();
	Возврат РезультатЗапроса.ВыгрузитьКолонку("Ссылка");
КонецФункции

Функция ПолучитьВсеЛНПоПациентуИЗастрахованному(СсылкаНаПациента, Застрахованный) Экспорт
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	ЛистНетрудоспособности.Ссылка
		|ИЗ
		|	Документ.ЛистНетрудоспособности КАК ЛистНетрудоспособности
		|ГДЕ
		|	ЛистНетрудоспособности.Пациент = &СсылкаНаПациента
		|	И ЛистНетрудоспособности.Застрахованный = &Застрахованный";
	Запрос.УстановитьПараметр("СсылкаНаПациента", СсылкаНаПациента);	
	Запрос.УстановитьПараметр("Застрахованный", Застрахованный);
	РезультатЗапроса = Запрос.Выполнить().Выгрузить();
	Возврат РезультатЗапроса.ВыгрузитьКолонку("Ссылка");
КонецФункции