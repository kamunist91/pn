﻿
Процедура ЗагрузитьОбновитьНоменклатуруМедицинскихУслуг(ТемпФайлV001XLSX) Экспорт
	Попытка
		НачатьТранзакцию();
		УстаревшиеВиды = Новый Массив;
		сч = 0;
		Создано = 0;
		Обновлено = 0;
		
	    ИмяЛиста = Новый Структура ("ИмяЛиста, НомерЛиста", "Лист1", 1);
		СтрокаЗаголовка = 1;
		НачСтрока = 2;
		КонСтрока = 0;
		КолвоСтрокExcel = 0;
		ТаблицаXLSX = РаботаСEXCEL.ЗагрузитьМетодом_MSEXCEL(ТемпФайлV001XLSX,
					ИмяЛиста, СтрокаЗаголовка, НачСтрока, КонСтрока, КолвоСтрокExcel);
					
		Для чс = 1 По ТаблицаXLSX.Количество() - 1 Цикл
			КодУсл = ТаблицаXLSX[чс].N1;
			НаименованиеУсл = ТаблицаXLSX[чс].N2;
			сч = сч + 1;
			Запрос = Новый Запрос;
			Запрос.Текст = 
				"ВЫБРАТЬ
				|	*
				|ИЗ
				|	Справочник.НоменклатураМедицинскихУслуг КАК НоменклатураМедицинскихУслуг
				|ГДЕ
				|	НоменклатураМедицинскихУслуг.КодУслуги = &КодУслуги";
			Запрос.УстановитьПараметр("КодУслуги", КодУсл);
			РезультатЗапроса = Запрос.Выполнить().Выбрать();
			Если РезультатЗапроса.Количество() = 1 Тогда
				РезультатЗапроса.Следующий();
				Услуга = РезультатЗапроса.Ссылка.ПолучитьОбъект();
				Обновлено = Обновлено + 1;
			Иначе
				Услуга = Справочники.НоменклатураМедицинскихУслуг.СоздатьЭлемент();
				Создано = Создано + 1;
			КонецЕсли;
			Услуга.КодУслуги = ?(ТипЗнч(КодУсл) = Тип("Строка"), КодУсл, Неопределено);
			Услуга.Наименование = ?(ТипЗнч(НаименованиеУсл) = Тип("Строка"), НаименованиеУсл, Неопределено);
			Услуга.ПолноеНаименование = ?(ТипЗнч(НаименованиеУсл) = Тип("Строка"), НаименованиеУсл, Неопределено);
			//Услуга.ДатаНачалаДействия = ?(ТипЗнч(Запись.DATEBEG) = Тип("Строка"),
			//						ПроверкиВводаКлиентСервер.ПреобразоватьСтрокуВДату(Запись.DATEBEG), Неопределено);
			//Услуга.ДатаОкончанияДействия = ?(ТипЗнч(Запись.DATEEND) = Тип("Строка"),
			//						ПроверкиВводаКлиентСервер.ПреобразоватьСтрокуВДату(Запись.DATEEND), Неопределено);
			Услуга.Записать();
			Если ЗначениеЗаполнено(Услуга.ДатаОкончанияДействия) И Услуга.ДатаОкончанияДействия < ТекущаяДата() Тогда
				УстаревшиеВиды.Добавить(Услуга.Ссылка);
			КонецЕсли;
		КонецЦикла;
		ЗафиксироватьТранзакцию();
		Сообщить("Запись номенклатуры медицинских услуг завершена!"
				+ Символы.ПС + "Записано - " + Строка(сч)
				+ ", из них устаревших записей - " + Строка(УстаревшиеВиды.Количество())
				+ Символы.ПС + "Из них обновлено - " + Строка(Обновлено) + ", "
				+ "создано новых записей - " + Строка(Создано));
		Для Каждого Устар Из УстаревшиеВиды Цикл
			Сообщить("Завершен срок действия: " + Строка(Устар));
		КонецЦикла;
	Исключение
		ОтменитьТранзакцию();
		Сообщить("Запись номенклатуры медицинских услуг не удалась!" + Символы.ПС + ОписаниеОшибки());
	КонецПопытки;
КонецПроцедуры

Функция ПолучитьУслугиПоСотруднику(Сотрудник) Экспорт
	Если ТипЗнч(Сотрудник) = Тип("СправочникСсылка.Сотрудники") Тогда
		Возврат Сотрудник.ОказываемыеУслуги.ВыгрузитьКолонку("Услуга");
	Иначе
		Возврат Новый Массив;
	КонецЕсли;
КонецФункции

Функция ПолучитьОграниченияТипаПоУслуге(Услуга, ДатаОтбора = Неопределено) Экспорт
	Если ДатаОтбора = Неопределено Тогда
		ДатаОтбора = ТекущаяДата();
	КонецЕсли;
	Если ТипЗнч(Услуга) = Тип("СправочникСсылка.НоменклатураМедицинскихУслуг") Тогда
		Запрос = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ
		               |	ТипыДокументовКУслугам.ТипДокумента
		               |ИЗ
		               |	РегистрСведений.ТипыДокументовКУслугам КАК ТипыДокументовКУслугам
		               |ГДЕ
		               |	ТипыДокументовКУслугам.Услуга = &УслугаПар И
		               |	 ТипыДокументовКУслугам.ДатаНачалаДействия <= &ДатаОтбора
		               |	И (ТипыДокументовКУслугам.ДатаОкончанияДействия >= &ДатаОтбора
		               |			ИЛИ ТипыДокументовКУслугам.ДатаОкончанияДействия = ДАТАВРЕМЯ(1, 1, 1, 0, 0, 0))";
		Запрос.УстановитьПараметр("УслугаПар", Услуга);
		Запрос.УстановитьПараметр("ДатаОтбора", ДатаОтбора);
		РезультатЗапроса = Запрос.Выполнить().Выгрузить();
		Если РезультатЗапроса.Количество() > 0 Тогда
			Возврат РезультатЗапроса.ВыгрузитьКолонку("ТипДокумента");
		Иначе
			Возврат Новый Массив;
		КонецЕсли;
	Иначе
		Возврат Новый Массив;
	КонецЕсли;
КонецФункции

Функция ПолучитьФормыДокументаПоУслуге(Услуга, ТипПротокола = Неопределено, ДатаОтбора = Неопределено) Экспорт
	Если ДатаОтбора = Неопределено Тогда
		ДатаОтбора = ТекущаяДата();
	КонецЕсли;
	Если ТипЗнч(Услуга) = Тип("СправочникСсылка.НоменклатураМедицинскихУслуг") Тогда
		Запрос = Новый Запрос;
		Запрос.Текст = "ВЫБРАТЬ
		               |	ТипыДокументовКУслугам.ФормаДокумента
		               |ИЗ
		               |	РегистрСведений.ТипыДокументовКУслугам КАК ТипыДокументовКУслугам
		               |ГДЕ
		               |	ТипыДокументовКУслугам.Услуга = &УслугаПар
		               |	И ТипыДокументовКУслугам.ДатаНачалаДействия <= &ДатаОтбора
		               |	И (ТипыДокументовКУслугам.ДатаОкончанияДействия >= &ДатаОтбора
		               |			ИЛИ ТипыДокументовКУслугам.ДатаОкончанияДействия = ДАТАВРЕМЯ(1, 1, 1, 0, 0, 0))";
		Если ТипПротокола <> Неопределено Тогда
			Запрос.Текст = Запрос.Текст
							+ "	И ТипыДокументовКУслугам.ТипДокумента = &ТипПротокола";
			Запрос.УстановитьПараметр("ТипПротокола", ТипПротокола);
		КонецЕсли;
		Запрос.УстановитьПараметр("УслугаПар", Услуга);
		Запрос.УстановитьПараметр("ДатаОтбора", ДатаОтбора);
		РезультатЗапроса = Запрос.Выполнить().Выгрузить();
		Если РезультатЗапроса.Количество() > 0 Тогда
			Возврат РезультатЗапроса.ВыгрузитьКолонку("ФормаДокумента");
		Иначе
			Возврат Новый Массив;
		КонецЕсли;
	Иначе
		Возврат Новый Массив;
	КонецЕсли;
КонецФункции

Функция ПолучитьПараметрыОтбораУслугПоСотруднику(Сотрудник) Экспорт
	Услуги = ПолучитьУслугиПоСотруднику(Сотрудник);	
	ЗначенияСсылок = Новый ФиксированныйМассив(Услуги);
	ПараметрыВыбораУслуг = Новый ПараметрВыбора("Отбор.Ссылка", ЗначенияСсылок);
	
	НовыйМассив = Новый Массив();
	НовыйМассив.Добавить(ПараметрыВыбораУслуг);
	НовыеПараметры = Новый ФиксированныйМассив(НовыйМассив);
	Возврат НовыеПараметры;
КонецФункции