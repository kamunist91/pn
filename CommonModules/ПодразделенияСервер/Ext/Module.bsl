﻿Функция ПолучитьКодыБалансодержателей() Экспорт
	Запрос = Новый Запрос;
	Запрос.Текст = 
		"ВЫБРАТЬ
		|	Подразделения.КодПодразделения
		|ИЗ
		|	Справочник.Подразделения КАК Подразделения
		|ГДЕ
		|	Подразделения.Балансодержатель = ИСТИНА";
	
	РезультатЗапроса = Запрос.Выполнить().Выгрузить();
	Если РезультатЗапроса.Количество() > 0 Тогда
		Возврат РезультатЗапроса.ВыгрузитьКолонку("КодПодразделения");
	Иначе
		Возврат Неопределено;
	КонецЕсли;
КонецФункции