﻿
Процедура ЗагрузитьОбновитьПрофили(ТемпФайлПрофилей) Экспорт
	XML = Новый ЧтениеXML;
	XML.ОткрытьФайл(ТемпФайлПрофилей);
	
	БлокЗаписей = Новый Массив;
	РеестрXDTO = ФабрикаXDTO.ПрочитатьXML(XML);
	
	Если ТипЗнч(РеестрXDTO.zap) <> Тип("СписокXDTO") Тогда
		БлокЗаписей.Добавить(РеестрXDTO.zap);	
	Иначе
		БлокЗаписей = РеестрXDTO.zap;
	КонецЕсли;
	Попытка
		НачатьТранзакцию();
		УстаревшиеПрофили = Новый Массив;
		сч = 0;
		Создано = 0;
		Обновлено = 0;
		Для Каждого Запись из БлокЗаписей Цикл
			сч = сч + 1;
			Запрос = Новый Запрос;
			Запрос.Текст = 
				"ВЫБРАТЬ
				|	*
				|ИЗ
				|	Справочник.ПрофилиМедицинскойПомощи КАК ПрофилиМедицинскойПомощи
				|ГДЕ
				|	ПрофилиМедицинскойПомощи.КодПрофиля = &КодПрофиля";
			Запрос.УстановитьПараметр("КодПрофиля", Запись.IDPR);
			РезультатЗапроса = Запрос.Выполнить().Выбрать();
			Если РезультатЗапроса.Количество() = 1 Тогда
				РезультатЗапроса.Следующий();
				Проф = РезультатЗапроса.Ссылка.ПолучитьОбъект();
				Обновлено = Обновлено + 1;
			Иначе
				Проф = Справочники.ПрофилиМедицинскойПомощи.СоздатьЭлемент();
				Создано = Создано + 1;
			КонецЕсли;
			Проф.КодПрофиля = ?(ТипЗнч(Запись.IDPR) = Тип("Строка"), Запись.IDPR, Неопределено);
			Проф.Наименование = ?(ТипЗнч(Запись.PRNAME) = Тип("Строка"), Запись.PRNAME, Неопределено);
			Проф.ПолноеНаименование = ?(ТипЗнч(Запись.PRNAME) = Тип("Строка"), Запись.PRNAME, Неопределено);
			Проф.ДатаНачалаДействия = ?(ТипЗнч(Запись.DATEBEG) = Тип("Строка"),
									ПроверкиВводаКлиентСервер.ПреобразоватьСтрокуВДату(Запись.DATEBEG), Неопределено);
			Проф.ДатаОкончанияДействия = ?(ТипЗнч(Запись.DATEEND) = Тип("Строка"),
									ПроверкиВводаКлиентСервер.ПреобразоватьСтрокуВДату(Запись.DATEEND), Неопределено);
			Проф.Записать();
			Если ЗначениеЗаполнено(Проф.ДатаОкончанияДействия) И Проф.ДатаОкончанияДействия < ТекущаяДата() Тогда
				УстаревшиеПрофили.Добавить(Проф.Ссылка);
			КонецЕсли;
		КонецЦикла;
		ЗафиксироватьТранзакцию();
		XML.Закрыть();
		Сообщить("Запись профилей завершена!"
				+ Символы.ПС + "Записано - " + Строка(сч)
				+ ", из них устаревших записей - " + Строка(УстаревшиеПрофили.Количество())
				+ Символы.ПС + "Из них обновлено - " + Строка(Обновлено) + ", "
				+ "создано новых записей - " + Строка(Создано));
		Для Каждого Устар Из УстаревшиеПрофили Цикл
			Сообщить("Завершен срок действия: " + Строка(Устар));
		КонецЦикла;
	Исключение
		ОтменитьТранзакцию();
		Сообщить("Запись профилей не удалась!" + Символы.ПС + ОписаниеОшибки());
	КонецПопытки;
КонецПроцедуры
