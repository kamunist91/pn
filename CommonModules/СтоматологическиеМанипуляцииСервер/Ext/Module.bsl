﻿
Процедура ЗагрузитьОбновитьСтоматологическиеМанипуляции(ТемпФайлМанипуляций) Экспорт
	XML = Новый ЧтениеXML;
	XML.ОткрытьФайл(ТемпФайлМанипуляций);
	
	БлокЗаписей = Новый Массив;
	РеестрXDTO = ФабрикаXDTO.ПрочитатьXML(XML);
	
	Если ТипЗнч(РеестрXDTO.zap) <> Тип("СписокXDTO") Тогда
		БлокЗаписей.Добавить(РеестрXDTO.zap);	
	Иначе
		БлокЗаписей = РеестрXDTO.zap;
	КонецЕсли;
	Попытка
		НачатьТранзакцию();
		УстаревшиеМанипуляции = Новый Массив;
		сч = 0;
		Создано = 0;
		Обновлено = 0;
		Для Каждого Запись из БлокЗаписей Цикл
			сч = сч + 1;
			Запрос = Новый Запрос;
			Запрос.Текст = 
				"ВЫБРАТЬ
				|	СтоматологическиеМанипуляции.Ссылка
				|ИЗ
				|	Справочник.СтоматологическиеМанипуляции КАК СтоматологическиеМанипуляции
				|ГДЕ
				|	СтоматологическиеМанипуляции.КодСтоматологическойМанипуляции = &КодСтоматологическойМанипуляции";
			Запрос.УстановитьПараметр("КодСтоматологическойМанипуляции", Запись.CODE);
			РезультатЗапроса = Запрос.Выполнить().Выбрать();
			Если РезультатЗапроса.Количество() = 1 Тогда
				РезультатЗапроса.Следующий();
				Манипуляция = РезультатЗапроса.Ссылка.ПолучитьОбъект();
				Обновлено = Обновлено + 1;
			Иначе
				Манипуляция = Справочники.СтоматологическиеМанипуляции.СоздатьЭлемент();
				Создано = Создано + 1;
			КонецЕсли;
			Манипуляция.КодСтоматологическойМанипуляции = ?(ТипЗнч(Запись.CODE) = Тип("Строка"), СокрЛП(Запись.CODE), Неопределено);
			Манипуляция.Наименование = ?(ТипЗнч(Запись.NAME) = Тип("Строка"), СокрЛП(Запись.NAME), Неопределено);
			Манипуляция.ПолноеНаименование = ?(ТипЗнч(Запись.NAME) = Тип("Строка"), СокрЛП(Запись.NAME), Неопределено);
			Манипуляция.УЕТДляВзрослогоНаселения = ?(ТипЗнч(Запись.UET_ADULT) = Тип("Строка"), Число(Запись.UET_ADULT), Неопределено);
			Манипуляция.УЕТДляДетскогоНаселения = ?(ТипЗнч(Запись.UET_BABY) = Тип("Строка"), Число(Запись.UET_BABY), Неопределено);
			Манипуляция.НеобходимостьУказанияЗуба = ?(ТипЗнч(Запись.Z) = Тип("Строка"), Число(Запись.Z), Неопределено);
			Манипуляция.ПереченьСоответствующихНомеровЗубов = ?(ТипЗнч(Запись.NZ) = Тип("Строка"), Запись.NZ, Неопределено);			
			Манипуляция.ДатаНачалаДействия = ?(ТипЗнч(Запись.START_DATE) = Тип("Строка"),
									ПроверкиВводаКлиентСервер.ПреобразоватьСтрокуВДату(Запись.START_DATE), Неопределено);
			Манипуляция.ДатаОкончанияДействия = ?(ТипЗнч(Запись.FINAL_DATE) = Тип("Строка"),
									ПроверкиВводаКлиентСервер.ПреобразоватьСтрокуВДату(Запись.FINAL_DATE), Неопределено);
			Манипуляция.ДатаДобавления = ?(ТипЗнч(Запись.ADD_DATE) = Тип("Строка"), ПроверкиВводаКлиентСервер.ПреобразоватьСтрокуВДату(Запись.ADD_DATE), Неопределено);
			
			Манипуляция.Записать();
			Если ЗначениеЗаполнено(Манипуляция.ДатаОкончанияДействия) И Манипуляция.ДатаОкончанияДействия < ТекущаяДата() Тогда
				УстаревшиеМанипуляции.Добавить(Манипуляция.Ссылка);
			КонецЕсли;
		КонецЦикла;
		ЗафиксироватьТранзакцию();
		XML.Закрыть();
		Сообщить("Запись стоматологических манипуляций завершена!"
				+ Символы.ПС + "Записано - " + Строка(сч)
				+ ", из них устаревших записей - " + Строка(УстаревшиеМанипуляции.Количество())
				+ Символы.ПС + "Из них обновлено - " + Строка(Обновлено) + ", "
				+ "создано новых записей - " + Строка(Создано));
		Для Каждого Устар Из УстаревшиеМанипуляции Цикл
			Сообщить("Завершен срок действия: " + Строка(Устар));
		КонецЦикла;
	Исключение
		ОтменитьТранзакцию();
		Сообщить("Запись стоматологических манипуляций не удалась!" + Символы.ПС + ОписаниеОшибки());
	КонецПопытки;
КонецПроцедуры
