﻿ 
Процедура ЗаполнениеФИО(ПолеФИО, ДобавочноеЗначение) Экспорт 
	Если ПолеФИО = "" Тогда
		ПолеФИО = ДобавочноеЗначение;
	Иначе
		ПолеФИО = ПолеФИО + " " + ДобавочноеЗначение;
	КонецЕсли;
КонецПроцедуры

Процедура ПроверитьПолПоОкончаниюОтчества(Отчество, Пол) Экспорт
	КоличествоСимволов = СтрДлина(Отчество);
	Если (Сред(Отчество, КоличествоСимволов - 2, 3) = "вич" И Строка(Пол) = "Женский")
		ИЛИ Сред(Отчество, КоличествоСимволов - 2, 3) = "вна" И Строка(Пол) = "Мужской" Тогда
		ПроверкиВводаКлиентСервер.ВывестиОшибку("Возможно неправильный пол!", "Пол", "Объект");
	КонецЕсли;
КонецПроцедуры

Функция АвтозаменаРусскихСимволовНаЛатинскиеПоКлавиатуре(ИсходнаяСтрока) Экспорт
	Если СтрДлина(ИсходнаяСтрока) > 0 Тогда
		НоваяСтрока = "";
		Алфавит = ПолучитьСоответствияСимволовАлфавита();
		Для сч = 1 По СтрДлина(ИсходнаяСтрока) Цикл
			Символ = Сред(ИсходнаяСтрока, сч, 1);
			Символ = ?(Алфавит.Получить(Символ) = Неопределено, Символ, Алфавит.Получить(Символ));
			НоваяСтрока = НоваяСтрока + Символ;
		КонецЦикла;
		Возврат НоваяСтрока;
	КонецЕсли;
КонецФункции

Функция ПолучитьСоответствияСимволовАлфавита() Экспорт
	Алфавит = Новый Соответствие;
	Алфавит.Вставить("а", "f");
	//Алфавит.Вставить("б", ",");
	Алфавит.Вставить("в", "d");
	Алфавит.Вставить("г", "u");
	Алфавит.Вставить("д", "l");
	Алфавит.Вставить("е", "t");
	//Алфавит.Вставить("ё", "`");
	//Алфавит.Вставить("ж", ";");
	Алфавит.Вставить("з", "p");
	Алфавит.Вставить("и", "b");
	Алфавит.Вставить("й", "q");
	Алфавит.Вставить("к", "r");
	Алфавит.Вставить("л", "k");
	Алфавит.Вставить("м", "v");
	Алфавит.Вставить("н", "y");
	Алфавит.Вставить("о", "j");
	Алфавит.Вставить("п", "g");
	Алфавит.Вставить("р", "h");
	Алфавит.Вставить("с", "c");
	Алфавит.Вставить("т", "n");
	Алфавит.Вставить("у", "e");
	Алфавит.Вставить("ф", "a");
	//Алфавит.Вставить("х", "[");
	Алфавит.Вставить("ц", "w");
	Алфавит.Вставить("ч", "x");
	Алфавит.Вставить("ш", "i");
	Алфавит.Вставить("щ", "o");
	//Алфавит.Вставить("ъ", "]");
	Алфавит.Вставить("ы", "s");
	Алфавит.Вставить("ь", "m");
	//Алфавит.Вставить("э", "'");
	//Алфавит.Вставить("ю", ".");
	Алфавит.Вставить("я", "z");
	Алфавит.Вставить("А", "F");
	//Алфавит.Вставить("Б", "<");
	Алфавит.Вставить("В", "D");
	Алфавит.Вставить("Г", "U");
	Алфавит.Вставить("Д", "L");
	Алфавит.Вставить("Е", "T");
	//Алфавит.Вставить("Ё", "~");
	//Алфавит.Вставить("Ж", ":");
	Алфавит.Вставить("З", "P");
	Алфавит.Вставить("И", "B");
	Алфавит.Вставить("Й", "Q");
	Алфавит.Вставить("К", "R");
	Алфавит.Вставить("Л", "K");
	Алфавит.Вставить("М", "V");
	Алфавит.Вставить("Н", "Y");
	Алфавит.Вставить("О", "J");
	Алфавит.Вставить("П", "G");
	Алфавит.Вставить("Р", "H");
	Алфавит.Вставить("С", "C");
	Алфавит.Вставить("Т", "N");
	Алфавит.Вставить("У", "E");
	Алфавит.Вставить("Ф", "A");
	//Алфавит.Вставить("Х", "{");
	Алфавит.Вставить("Ц", "W");
	Алфавит.Вставить("Ч", "X");
	Алфавит.Вставить("Ш", "I");
	Алфавит.Вставить("Щ", "O");
	//Алфавит.Вставить("Ъ", "}");
	Алфавит.Вставить("Ы", "S");
	Алфавит.Вставить("Ь", "M");
	//Алфавит.Вставить("Э", """);
	//Алфавит.Вставить("Ю", ">");
	Алфавит.Вставить("Я", "Z");
	Возврат Алфавит;
КонецФункции