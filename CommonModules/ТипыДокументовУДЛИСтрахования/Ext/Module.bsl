﻿
Процедура ЗагрузитьОбновитьВидыДокументов(ТемпФайлТиповДокументовУДЛ) Экспорт
	XML = Новый ЧтениеXML;
	XML.ОткрытьФайл(ТемпФайлТиповДокументовУДЛ);
	
	БлокЗаписей = Новый Массив;
	РеестрXDTO = ФабрикаXDTO.ПрочитатьXML(XML);
	
	Если ТипЗнч(РеестрXDTO.zap) <> Тип("СписокXDTO") Тогда
		БлокЗаписей.Добавить(РеестрXDTO.zap);	
	Иначе
		БлокЗаписей = РеестрXDTO.zap;
	КонецЕсли;
	Попытка
		НачатьТранзакцию();
		УстаревшиеТипыДокументов = Новый Массив;
		сч = 0;
		Создано = 0;
		Обновлено = 0;
		Для Каждого Запись из БлокЗаписей Цикл
			сч = сч + 1;
			Запрос = Новый Запрос;
			Запрос.Текст = 
				"ВЫБРАТЬ
				|	*
				|ИЗ
				|	Справочник.ВидыДокументов КАК ВидыДокументов
				|ГДЕ
				|	ВидыДокументов.КодВидаДокумента = &КодВидаДокумента
				|	И НЕ ВидыДокументов.Наименование ПОДОБНО &Полис
				|	И НЕ ВидыДокументов.Наименование ПОДОБНО &Состояние";
			Запрос.УстановитьПараметр("КодВидаДокумента", Запись.IDDoc);
			Запрос.УстановитьПараметр("Полис", "%Полис%");
			Запрос.УстановитьПараметр("Состояние", "%Состояние на учёте%");
			РезультатЗапроса = Запрос.Выполнить().Выбрать();
			Если РезультатЗапроса.Количество() = 1 Тогда
				РезультатЗапроса.Следующий();
				ТипДока = РезультатЗапроса.Ссылка.ПолучитьОбъект();
				Обновлено = Обновлено + 1;
			Иначе
				ТипДока = Справочники.ВидыДокументов.СоздатьЭлемент();
				Создано = Создано + 1;
			КонецЕсли;
			ТипДока.КодВидаДокумента = ?(ТипЗнч(Запись.IDDoc) = Тип("Строка"), Запись.IDDoc, Неопределено);
			ТипДока.Наименование = ?(ТипЗнч(Запись.DocName) = Тип("Строка"), Запись.DocName, Неопределено);
			//Не трогаем маски серии и номера, много завязок в коде
			//Серия
			//Маска
			Если ТипЗнч(Запись.DATEBEG) = Тип("Строка") Тогда
				ТипДока.ДатаНачалаДействия = ПроверкиВводаКлиентСервер.ПреобразоватьСтрокуВДату(Запись.DATEBEG);
			КонецЕсли;
			Если ТипЗнч(Запись.DATEEND) = Тип("Строка") Тогда
				ТипДока.ДатаОкончанияДействия = ПроверкиВводаКлиентСервер.ПреобразоватьСтрокуВДату(Запись.DATEEND);
			КонецЕсли;
			ТипДока.Автор = "Загрузка/Обновление " + Формат(ТекущаяДата(), "ДЛФ=Д");
			ТипДока.Записать();
			Если ЗначениеЗаполнено(ТипДока.ДатаОкончанияДействия) И ТипДока.ДатаОкончанияДействия < ТекущаяДата() Тогда
				УстаревшиеТипыДокументов.Добавить(ТипДока.Ссылка);
			КонецЕсли;
		КонецЦикла;
		ЗафиксироватьТранзакцию();
		XML.Закрыть();
		Сообщить("Запись видов (типов) документов УДЛ завершена!"
				+ Символы.ПС + "Записано - " + Строка(сч)
				+ ", из них устаревших записей - " + Строка(УстаревшиеТипыДокументов.Количество())
				+ Символы.ПС + "Из них обновлено - " + Строка(Обновлено) + ", "
				+ "создано новых записей - " + Строка(Создано));
		Для Каждого Устар Из УстаревшиеТипыДокументов Цикл
			Сообщить("Завершен срок действия: " + Строка(Устар));
		КонецЦикла;
	Исключение
		ОтменитьТранзакцию();
		Сообщить("Запись видов (типов) документов УДЛ не удалась!" + Символы.ПС + ОписаниеОшибки());
	КонецПопытки;
КонецПроцедуры

Процедура ЗагрузитьОбновитьВидыДокументовСтрахования(ТемпФайлТиповДокументовСтрахования) Экспорт
	XML = Новый ЧтениеXML;
	XML.ОткрытьФайл(ТемпФайлТиповДокументовСтрахования);
	
	БлокЗаписей = Новый Массив;
	РеестрXDTO = ФабрикаXDTO.ПрочитатьXML(XML);
	
	Если ТипЗнч(РеестрXDTO.zap) <> Тип("СписокXDTO") Тогда
		БлокЗаписей.Добавить(РеестрXDTO.zap);	
	Иначе
		БлокЗаписей = РеестрXDTO.zap;
	КонецЕсли;
	Попытка
		НачатьТранзакцию();
		УстаревшиеТипыДокументов = Новый Массив;
		сч = 0;
		Создано = 0;
		Обновлено = 0;
		Для Каждого Запись из БлокЗаписей Цикл
			сч = сч + 1;
			Запрос = Новый Запрос;
			Запрос.Текст = 
				"ВЫБРАТЬ
				|	*
				|ИЗ
				|	Справочник.ВидыДокументов КАК ВидыДокументов
				|ГДЕ
				|	ВидыДокументов.КодВидаДокумента = &КодВидаДокумента
				|	И ВидыДокументов.Наименование ПОДОБНО &Полис";
			Запрос.УстановитьПараметр("КодВидаДокумента", Запись.IDDOC);
			Запрос.УстановитьПараметр("Полис", "%Полис%");
			РезультатЗапроса = Запрос.Выполнить().Выбрать();
			Если РезультатЗапроса.Количество() = 1 Тогда
				РезультатЗапроса.Следующий();
				ТипДока = РезультатЗапроса.Ссылка.ПолучитьОбъект();
				Обновлено = Обновлено + 1;
			Иначе
				ТипДока = Справочники.ВидыДокументов.СоздатьЭлемент();
				Создано = Создано + 1;
			КонецЕсли;
			ТипДока.КодВидаДокумента = ?(ТипЗнч(Запись.IDDOC) = Тип("Строка"), Запись.IDDOC, Неопределено);
			ТипДока.Наименование = ?(ТипЗнч(Запись.DOCNAME) = Тип("Строка"), Запись.DOCNAME, Неопределено);
			//Не трогаем маски серии и номера, много завязок в коде
			//Серия
			//Маска
			Если ТипЗнч(Запись.DATEBEG) = Тип("Строка") Тогда
				ТипДока.ДатаНачалаДействия = ПроверкиВводаКлиентСервер.ПреобразоватьСтрокуВДату(Запись.DATEBEG);
			КонецЕсли;
			Если ТипЗнч(Запись.DATEEND) = Тип("Строка") Тогда
				ТипДока.ДатаОкончанияДействия = ПроверкиВводаКлиентСервер.ПреобразоватьСтрокуВДату(Запись.DATEEND);
			КонецЕсли;
			ТипДока.Автор = "Загрузка/Обновление " + Формат(ТекущаяДата(), "ДЛФ=Д");
			ТипДока.Записать();
			Если ЗначениеЗаполнено(ТипДока.ДатаОкончанияДействия) И ТипДока.ДатаОкончанияДействия < ТекущаяДата() Тогда
				УстаревшиеТипыДокументов.Добавить(ТипДока.Ссылка);
			КонецЕсли;
		КонецЦикла;
		ЗафиксироватьТранзакцию();
		XML.Закрыть();
		Сообщить("Запись видов (типов) документов страхования завершена!"
				+ Символы.ПС + "Записано - " + Строка(сч)
				+ ", из них устаревших записей - " + Строка(УстаревшиеТипыДокументов.Количество())
				+ Символы.ПС + "Из них обновлено - " + Строка(Обновлено) + ", "
				+ "создано новых записей - " + Строка(Создано));
		Для Каждого Устар Из УстаревшиеТипыДокументов Цикл
			Сообщить("Завершен срок действия: " + Строка(Устар));
		КонецЦикла;
	Исключение
		ОтменитьТранзакцию();
		Сообщить("Запись видов (типов) документов страхования не удалась!" + Символы.ПС + ОписаниеОшибки());
	КонецПопытки;
КонецПроцедуры