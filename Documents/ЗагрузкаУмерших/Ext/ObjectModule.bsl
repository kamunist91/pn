﻿
Процедура ОбработкаПроведения(Отказ, РежимПроведения)
	Для Каждого Пациент Из Умершие Цикл 
		 ОбъектПациента = Пациент.СсылкаНаПациента.ПолучитьОбъект();
		 ОбъектПациента.ДатаСмерти = Пациент.ДатаСмерти;
		 ОбъектПациента.Прикреплен = Ложь;
		 ОбъектПациента.АвторЗаписи = "Загрузка умерших от "+ДокументЗагрузки.ДатаФайла; 
		 ОбъектПациента.ДатаИзменения = ТекущаяДата();
		 Пациент.ИсторияПередЗагрузкой = ИсторияПациентовСервер.ПолучитьПоследнююЗаписьПациентаВИстории(ОбъектПациента.Ссылка).ГУИДИстории; 
		 Пациент.Комментарий = "Загружен";
		 ОбъектПациента.Записать();
		 Пациент.ИсторияПослеЗагрузки = ИсторияПациентовСервер.ПолучитьПоследнююЗаписьПациентаВИстории(ОбъектПациента.Ссылка).ГУИДИстории;
		 ОбъектПациента.Записать(); 
	КонецЦикла;  
КонецПроцедуры

Процедура ОбработкаУдаленияПроведения(Отказ)
	Для Каждого Пациент Из Умершие Цикл
		ИсторияПациентовСервер.ВосстановлениеИзИсторииПоГУИДу(Пациент.ИсторияПередЗагрузкой);
		Пациент.Комментарий = "";
	КонецЦикла;	
КонецПроцедуры
