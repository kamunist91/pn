﻿#Область При_создании

&НаСервере
Процедура ПриСозданииНаСервере(Отказ, СтандартнаяОбработка)
	//Новый случай
	Элементы.Результат.Видимость = Ложь;
	Элементы.Переоткрыть.Видимость = Ложь;
	Если Параметры.Ключ.Пустая() Тогда
		
		Элементы.РезультатЛечения.Доступность = Ложь;
		//Передача ссылки на Пациента
		Если Параметры.Свойство("Пациент") Тогда
		    Объект.Пациент = Параметры.Пациент;
			Элементы.ВидСлучая.ПараметрыВыбора = СлучаиСервер.ПолучитьПараметрыОтбораВидовСлучаяПоПрикреплению(ПациентыСервер.ПрикрепленВТФОМС(Объект.Пациент));
			Элементы.ВидФинансирования.ПараметрыВыбора = ВидыФинансированияСервер.ПолучитьПараметрыОтбораДляВидовФинансирования();
			//Получение пользовательских настроек
			Настройки = НастройкиПользователяДляОформленияСлучаев.ПолучитьПользовательскиеНастройки(ТекущаяДата(),
						ПараметрыСеанса.ТекущийПользователь, ПараметрыСеанса.Подразделение, ПараметрыСеанса.Должность,
						ПараметрыСеанса.Специальность);
			Если Настройки <> Неопределено И ЗначениеЗаполнено(Настройки.ВидСлучая) Тогда
				Если Элементы.ВидСлучая.ПараметрыВыбора[0].Значение.Найти(Настройки.ВидСлучая) <> Неопределено Тогда
					Объект.ВидСлучая = Настройки.ВидСлучая;
				КонецЕсли;
				Элементы.ВидФинансирования.ПараметрыВыбора = ВидыФинансированияСервер.ПолучитьПараметрыОтбораДляВидовФинансирования();
				Элементы.МетодОплаты.ПараметрыВыбора = МетодыОплатыПоликлиническихСлучаевСервер.ПолучитьПараметрыОтбораМетодовПоВидуСлучаев(Настройки.ВидСлучая);
				Элементы.УсловияОказания.ПараметрыВыбора = УсловияОказанияМедицинскойПомощиСервер.ПолучитьПараметрыОтбораУсловийОказанияПоВидуСлучаев(Настройки.ВидСлучая);
				Элементы.ВидМедицинскойПомощи.ПараметрыВыбора = ВидыМедицинскойПомощиСервер.ПолучитьПараметрыОтбораВидовМППоВидуСлучаев(Настройки.ВидСлучая);
				Объект.ВидФинансирования = Настройки.ВидФинансирования;
				Если Элементы.УсловияОказания.ПараметрыВыбора[0].Значение.Найти(Настройки.УсловияОказания) <> Неопределено Тогда
					Объект.УсловияОказания = Настройки.УсловияОказания;
				КонецЕсли;
				Если Элементы.ВидМедицинскойПомощи.ПараметрыВыбора[0].Значение.Найти(Настройки.ВидМедицинскойПомощи) <> Неопределено Тогда
					Объект.ВидМедицинскойПомощи = Настройки.ВидМедицинскойПомощи;
				КонецЕсли;
			КонецЕсли;
							
			Объект.Организация = Справочники.Организации.НайтиПоКоду("000000001");		
			
			//ГУИДы
			Объект.ГУИД = ФормированиеГУИДОВСервер.СоздатьГУИД1С();

			//авторы записей
			Объект.Автор = ИмяПользователя();
			Объект.АвторПользователь = ПараметрыСеанса.ТекущийПользователь;
		Иначе
			УсловноеОформлениеФормы();
			Отказ = Истина;
			Сообщить("Не удалось определить пациента!");
		КонецЕсли;
	Иначе
		Элементы.ВидСлучая.ПараметрыВыбора = СлучаиСервер.ПолучитьПараметрыОтбораВидовСлучаяПоПрикреплению(ПациентыСервер.ПрикрепленВТФОМС(Объект.Пациент));
		Элементы.ВидФинансирования.ПараметрыВыбора = ВидыФинансированияСервер.ПолучитьПараметрыОтбораДляВидовФинансирования();
		Если ЗначениеЗаполнено(Объект.ВидСлучая) Тогда
			Элементы.МетодОплаты.ПараметрыВыбора = МетодыОплатыПоликлиническихСлучаевСервер.ПолучитьПараметрыОтбораМетодовПоВидуСлучаев(Объект.ВидСлучая);
			Элементы.УсловияОказания.ПараметрыВыбора = УсловияОказанияМедицинскойПомощиСервер.ПолучитьПараметрыОтбораУсловийОказанияПоВидуСлучаев(Объект.ВидСлучая);
			Элементы.ВидМедицинскойПомощи.ПараметрыВыбора = ВидыМедицинскойПомощиСервер.ПолучитьПараметрыОтбораВидовМППоВидуСлучаев(Объект.ВидСлучая);
		КонецЕсли;
		
		Если Объект.Проведен Тогда
			Элементы.Результат.Видимость = Истина;
			Элементы.Переоткрыть.Видимость = Истина;
		КонецЕсли;
		Элементы.РезультатЛечения.Доступность = Истина;
		Если ЗначениеЗаполнено(Объект.ВидТравмы) Тогда
			Элементы.ВидТравмы.Видимость = Истина;
		КонецЕсли;
		Объект.Автор = ИмяПользователя();
		
		//Только для Администратора
		Если ПользователиИнформационнойБазы.ТекущийПользователь().Имя = "Администратор" Тогда
			Для Каждого Эл Из Элементы Цикл
				Попытка
					Если Эл.Вид = ВидПоляФормы.ПолеВвода Тогда
						Эл.Доступность = Истина;
						Эл.ТолькоПросмотр = Ложь;
						Эл.БыстрыйВыбор = Ложь;
						Попытка
							Если Эл.ПараметрыВыбора.Количество() > 0 Тогда 
								НовыйМассив = Новый Массив;
								НовыеПараметры = Новый ФиксированныйМассив(НовыйМассив);
								Эл.ПараметрыВыбора = НовыеПараметры;
							КонецЕсли;
						Исключение
						КонецПопытки;
					КонецЕсли;
				Исключение
				КонецПопытки;
			КонецЦикла;
		КонецЕсли;
		
	КонецЕсли;	
КонецПроцедуры

#КонецОбласти

#Область УО

&НаСервере
Процедура УсловноеОформлениеФормы()
	//Посещения
	Посещения.Отбор.Элементы.Очистить();
	ЭлементОтбора = Посещения.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	ЭлементОтбора.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Случай");
	ЭлементОтбора.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	ЭлементОтбора.Использование = Истина;
	ЭлементОтбора.ПравоеЗначение = Объект.Ссылка;
	////Услуги
	//ОказанныеУслуги.Отбор.Элементы.Очистить();
	//ЭлементОтбора2 = ОказанныеУслуги.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	//ЭлементОтбора2.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Случай");
	//ЭлементОтбора2.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	//ЭлементОтбора2.Использование = Истина;
	//ЭлементОтбора2.ПравоеЗначение = Объект.Ссылка;
	////Диагнозы
	//ДиагностированныеЗаболевания.Отбор.Элементы.Очистить();
	//ЭлементОтбора3 = ДиагностированныеЗаболевания.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	//ЭлементОтбора3.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Пациент");
	//ЭлементОтбора3.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	//ЭлементОтбора3.Использование = Истина;
	//ЭлементОтбора3.ПравоеЗначение = Объект.Пациент;
	//
	//ЭлементОтбора4 = ДиагностированныеЗаболевания.Отбор.Элементы.Добавить(Тип("ЭлементОтбораКомпоновкиДанных"));
	//ЭлементОтбора4.ЛевоеЗначение = Новый ПолеКомпоновкиДанных("Случай");
	//ЭлементОтбора4.ВидСравнения = ВидСравненияКомпоновкиДанных.Равно;
	//ЭлементОтбора4.Использование = Истина;
	//ЭлементОтбора4.ПравоеЗначение = Объект.Ссылка;
КонецПроцедуры

#КонецОбласти

#Область При_открытии

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	УсловноеОформлениеФормы()
КонецПроцедуры

#КонецОбласти

#Область Случай

#Область Изменение_пациента_в_случае

&НаКлиенте
Процедура ПациентРасширеннаяПодсказкаНажатие(Элемент)
	Если Объект.Ссылка.Пустая() Тогда
		Сообщить("Данный случай еще не сохранен." + Символы.ПС + "Вы можете закрыть и создать случай с нужным пациентом!");
	Иначе
		ОповещениеВыбораПациента = Новый ОписаниеОповещения("ФормаВыбораПациентаНаИзменениеЗавершение", ЭтотОбъект);
		ОткрытьФорму("Справочник.Пациенты.Форма.ФормаВыбора",,,,,,
					ОповещениеВыбораПациента,РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ФормаВыбораПациентаНаИзменениеЗавершение(Результат, ДополнительныеПараметры) Экспорт
	Если Результат <> Неопределено Тогда
		СлучаиСервер.ИзменитьПациентаВСлучае(Объект.Ссылка, Результат);
		Прочитать();
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура ВидСлучаяНачалоВыбора(Элемент, ДанныеВыбора, СтандартнаяОбработка)
	//Список = Новый СписокЗначений;
	//Если ПациентыСервер.ПрикрепленВТФОМС(Объект.Пациент) Тогда
	//	Список.ЗагрузитьЗначения(СлучаиСервер.ПолучитьИспользуемыеВидыСлучаев());
	//Иначе
	//	Список.ЗагрузитьЗначения(СлучаиСервер.ПолучитьИспользуемыеВидыСлучаевДляНеприкрепленного());
	//КонецЕсли;
	//ДанныеВыбора = Список;
	//СтандартнаяОбработка = Ложь;
КонецПроцедуры

&НаКлиенте
Процедура ВидСлучаяОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Если ВыбранноеЗначение <> Неопределено Тогда
		Объект.МетодОплаты = Неопределено;
		Объект.УсловияОказания = Неопределено;
		Объект.ВидМедицинскойПомощи = Неопределено;
		Элементы.МетодОплаты.ПараметрыВыбора = МетодыОплатыПоликлиническихСлучаевСервер.ПолучитьПараметрыОтбораМетодовПоВидуСлучаев(ВыбранноеЗначение);
		Элементы.УсловияОказания.ПараметрыВыбора = УсловияОказанияМедицинскойПомощиСервер.ПолучитьПараметрыОтбораУсловийОказанияПоВидуСлучаев(ВыбранноеЗначение);
		Элементы.ВидМедицинскойПомощи.ПараметрыВыбора = ВидыМедицинскойПомощиСервер.ПолучитьПараметрыОтбораВидовМППоВидуСлучаев(ВыбранноеЗначение);
		
		//Результаты
		Если Строка(ВыбранноеЗначение) = "Случай поликлинический" Тогда
			Элементы.РезультатОбращения.ОграничениеТипа = Новый ОписаниеТипов("СправочникСсылка.РезультатыОбращения");
		ИначеЕсли Строка(ВыбранноеЗначение) = "Случай диспансеризации" Тогда
			Элементы.РезультатОбращения.ОграничениеТипа = Новый ОписаниеТипов("СправочникСсылка.РезультатыДиспансеризации");
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

#Область Расширенная_подсказка_установка_значения_по_умолчанию

&НаКлиенте
Процедура ВидСлучаяРасширеннаяПодсказкаНажатие(Элемент)
	Если ЗначениеЗаполнено(Объект.ВидСлучая) Тогда
		Результат = НастройкиПользователяДляОформленияСлучаев.УстановитьЗначениеПоУмолчанию(ТекущаяДата(),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("ДатаПриема"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("ТекущийПользователь"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("Подразделение"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("Должность"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("Специальность"), "ВидСлучая", Объект.ВидСлучая);
		Если Результат Тогда
			Сообщить("Значение по умолчанию установлено");
		Иначе
			Сообщить("Запись не удалась!");
		КонецЕсли;
	Иначе
		Сообщить("Вы не заполнили Вид случая");
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ВидФинансированияРасширеннаяПодсказкаНажатие(Элемент)
	Если ЗначениеЗаполнено(Объект.ВидФинансирования) Тогда
		Результат = НастройкиПользователяДляОформленияСлучаев.УстановитьЗначениеПоУмолчанию(ТекущаяДата(),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("ДатаПриема"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("ТекущийПользователь"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("Подразделение"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("Должность"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("Специальность"),
			"ВидФинансирования", Объект.ВидФинансирования);
		Если Результат Тогда
			Сообщить("Значение по умолчанию установлено");
		Иначе
			Сообщить("Запись не удалась!");
		КонецЕсли;
	Иначе
		Сообщить("Вы не заполнили Вид финансирования");
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура УсловияОказанияРасширеннаяПодсказкаНажатие(Элемент)
	Если ЗначениеЗаполнено(Объект.УсловияОказания) Тогда
		Результат = НастройкиПользователяДляОформленияСлучаев.УстановитьЗначениеПоУмолчанию(ТекущаяДата(),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("ДатаПриема"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("ТекущийПользователь"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("Подразделение"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("Должность"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("Специальность"),
			"УсловияОказания", Объект.УсловияОказания);
		Если Результат Тогда
			Сообщить("Значение по умолчанию установлено");
		Иначе
			Сообщить("Запись не удалась!");
		КонецЕсли;
	Иначе
		Сообщить("Вы не заполнили Условия оказания медицинской помощи");
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ВидМедицинскойПомощиРасширеннаяПодсказкаНажатие(Элемент)
	Если ЗначениеЗаполнено(Объект.ВидМедицинскойПомощи) Тогда
		Результат = НастройкиПользователяДляОформленияСлучаев.УстановитьЗначениеПоУмолчанию(ТекущаяДата(),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("ДатаПриема"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("ТекущийПользователь"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("Подразделение"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("Должность"),
			ПараметрыСеансаСервер.ПолучитьЗначениеПараметраСеансаПоНаименованию("Специальность"),
			"ВидМедицинскойПомощи", Объект.ВидМедицинскойПомощи);
		Если Результат Тогда
			Сообщить("Значение по умолчанию установлено");
		Иначе
			Сообщить("Запись не удалась!");
		КонецЕсли;
	Иначе
		Сообщить("Вы не заполнили Вид медицинской помощи");
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура ВнутреннийНомерОкончаниеВводаТекста(Элемент, Текст, ДанныеВыбора, ПараметрыПолученияДанных, СтандартнаяОбработка)
	Соо = Новый Соответствие;
	Соо.Вставить(" ", "");
	//Соо.Вставить("_", "");
	//Соо.Вставить("!", "");
	//Соо.Вставить(Символ(34), "");
	//Соо.Вставить("`", "");
	//Соо.Вставить("~", "");
	//Соо.Вставить("@", "");
	//Соо.Вставить("#", "");
	//Соо.Вставить("$", "");
	//Соо.Вставить("%", "");
	//Соо.Вставить("^", "");
	//Соо.Вставить("&", "");
	//Соо.Вставить("*", "");
	//Соо.Вставить("(", "");
	//Соо.Вставить(")", "");
	//Соо.Вставить("+", "");
	//Соо.Вставить("=", "");
	//Соо.Вставить("<", "");
	//Соо.Вставить(">", "");
	//Соо.Вставить("?", "");
	//Соо.Вставить("'", "");
	//Соо.Вставить("}", "");
	//Соо.Вставить("{", "");
	//Соо.Вставить("[", "");
	//Соо.Вставить("]", "");
	//Соо.Вставить(".", "");
	//Соо.Вставить(",", "");
	//Соо.Вставить("№", "");
	//Соо.Вставить(";", "");
	//Соо.Вставить(":", "");
	НовТекст = СтроковыеФункцииСервер.УбратьИлиЗаменитьСимволыВСтроке(Соо, Текст);
	Объект.ВнутреннийНомер = НовТекст;
КонецПроцедуры

&НаКлиенте
Процедура МетодОплатыОбработкаВыбора(Элемент, ВыбранноеЗначение, СтандартнаяОбработка)
	Объект.ФормаОказания = Неопределено;
	КодМетодаОплаты = МетодыОплатыПоликлиническихСлучаевСервер.ПолучитьКодМетодаОплаты(ВыбранноеЗначение);
	Если КодМетодаОплаты = "8"
		ИЛИ КодМетодаОплаты = "10.2" Тогда
		Объект.ФормаОказания = ФормыОказанияМедицинскойПомощиСервер.ПолучитьФормуОказанияПоКоду("2");
		Элементы.ФормаОказания.Доступность = Ложь;
	Иначе
		Объект.ФормаОказания = ФормыОказанияМедицинскойПомощиСервер.ПолучитьФормуОказанияПоКоду("3");
		Элементы.ФормаОказания.Доступность = Ложь;
	//Иначе
	//	Если Строка(Объект.ВидСлучая) = "Случай диспансеризации" Тогда
	//		Объект.ФормаОказания = ФормыОказанияМедицинскойПомощиСервер.ПолучитьФормуОказанияПоКоду("3");
	//		Элементы.ФормаОказания.Доступность = Ложь;
	//	Иначе
	//		//получить без экстренной формы параметры выбора
	//		Элементы.ФормаОказания.ПараметрыВыбора = ФормыОказанияМедицинскойПомощиСервер.ПолучитьПараметрыОтбораФормОказанияПироговки();
	//	КонецЕсли;
	//	Элементы.ФормаОказания.Доступность = Истина;
	КонецЕсли;
КонецПроцедуры

#Область Запись

&НаКлиенте
Процедура ПередЗаписью(Отказ, ПараметрыЗаписи)
	Если Не ПроверитьЗаполнение() Тогда
		Отказ = Истина;
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

#КонецОбласти

#Область Посещения

#Область Перенос_посещения_на_другой_случай

&НаКлиенте
Процедура ПеренестиПосещение(Команда)
	Если Элементы.Посещения.ТекущиеДанные <> Неопределено Тогда
		ОтборСлучаев = Новый Структура;
		ОтборСлучаев.Вставить("Пациент", Объект.Пациент);
		ОтборСлучаев.Вставить("ДатаЗакрытия", Дата(1,1,1));
		ПараметрыВыбораСлучая = Новый Структура("Отбор, ДопПараметр", ОтборСлучаев, Объект.Ссылка);
		Описание = Новый ОписаниеОповещения("ВыборСлучаяПереносаЗавершение", ЭтотОбъект);
		ОткрытьФорму("Документ.Случай.Форма.ФормаВыбора", ПараметрыВыбораСлучая, ЭтотОбъект,,,,
					Описание, РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
	Иначе
		Сообщить("Не выбрано посещение!");
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ВыборСлучаяПереносаЗавершение(Результат, ДополнительныеПараметры) Экспорт
	Если Результат <> Неопределено Тогда
		ПосещенияСервер.ПеренестиПосещениеНаДругойСлучай(Элементы.Посещения.ТекущаяСтрока, Результат);
		ОповеститьОбИзменении(Элементы.Посещения.ТекущаяСтрока);
	Иначе
		Сообщить("Вы не выбрали открытый случай!");
	КонецЕсли;
КонецПроцедуры

#КонецОбласти

&НаКлиенте
Процедура ПосещенияПередНачаломДобавления(Элемент, Отказ, Копирование, Родитель, Группа, Параметр)
	Если Объект.Ссылка.Пустая() Тогда
		ОповещениеВопроса = Новый ОписаниеОповещения("ВопросСохраненияСлучаяЗавершение", ЭтотОбъект);
		ПоказатьВопрос(ОповещениеВопроса,
						"Данные случая не сохранены." + Символы.ПС + "Хотите сохранить и продолжить?",
						РежимДиалогаВопрос.ДаНет, 0, КодВозвратаДиалога.Нет);
		Отказ = Истина;
		Возврат;
	КонецЕсли;
	Если Копирование Тогда
		Отказ = Истина;
		Сообщить("Копирование запрещено!");
		Возврат;
	Иначе
		ДанныеПосещения = Новый Структура;
		ДанныеПосещения.Вставить("Случай", Объект.Ссылка);
		ДанныеПосещения.Вставить("МетодОплаты", Объект.МетодОплаты);
		ДанныеПосещения.Вставить("ВидСлучая", Объект.ВидСлучая);
		ПараметрыФормы = Новый Структура("ДанныеПосещения", ДанныеПосещения);
		
		ОткрытьФорму("Документ.Посещение.Форма.ФормаПосещения", ПараметрыФормы, ЭтотОбъект,,,,,РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
		Отказ = Истина;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ВопросСохраненияСлучаяЗавершение(РезультатВопроса, ДополнительныеПараметры) Экспорт
	Если РезультатВопроса = КодВозвратаДиалога.Да Тогда
		Если Записать() Тогда
			ДанныеПосещения = Новый Структура;
			ДанныеПосещения.Вставить("Случай", Объект.Ссылка);
			ДанныеПосещения.Вставить("МетодОплаты", Объект.МетодОплаты);
			ДанныеПосещения.Вставить("ВидСлучая", Объект.ВидСлучая);
			
			ПараметрыФормы = Новый Структура("ДанныеПосещения", ДанныеПосещения);
			ОткрытьФорму("Документ.Посещение.Форма.ФормаПосещения", ПараметрыФормы, ЭтотОбъект,,,,,РежимОткрытияОкнаФормы.БлокироватьОкноВладельца);
		Иначе
			Сообщить("Не удалось записать случай!");
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПосещенияПриИзменении(Элемент)
	Прочитать();
КонецПроцедуры

&НаКлиенте
Процедура ПослеЗаписи(ПараметрыЗаписи)
	//Установка отборов в динамические списки
	УсловноеОформлениеФормы();
	Элементы.РезультатЛечения.Доступность = Истина;
КонецПроцедуры

&НаКлиенте
Процедура ПосещенияПередУдалением(Элемент, Отказ)
	Если Элементы.Посещения.ТекущиеДанные <> Неопределено Тогда
		Ссылка = Элементы.Посещения.ТекущаяСтрока;
		ОповещениеВопроса = Новый ОписаниеОповещения("ВопросУдаленияЗависимыхОтПосещенияДанных", ОписанияОповещений, Ссылка);
		ПоказатьВопрос(ОповещениеВопроса,
						"При удалении посещения будут удалены также все услуги, протоколы и диагнозы"
						+ Символы.ПС + "Хотите продолжить удаление?",
						РежимДиалогаВопрос.ДаНет, 0, КодВозвратаДиалога.Нет);
	Иначе
		Сообщить("Не выбрано посещение!");
	КонецЕсли;
	Отказ = Истина;
КонецПроцедуры

#КонецОбласти

#Область Закрытие_случая

&НаКлиенте
Процедура РезультатЛечения(Команда)
	//Сначала проверка возможности закрытия в зависимости от вида случая метода оплаты и других условий
	Если Записать() Тогда
		РезультатыПроверки = СлучаиСервер.ПроверитьСлучайПередВводомРезультата(Объект.Ссылка);
		Если Не РезультатыПроверки.Отказ Тогда
		    Элементы.Результат.Видимость = Истина;
			ТекущийЭлемент = Элементы.Результат;
			Объект.ДатаЗакрытия = ТекущаяДата();
			Попытка
				ДанныеДиагноза = СлучаиСервер.ПолучитьЗаключительныйДиагнозСлучаяСтруктурой(Объект.Ссылка);
				Объект.ЗаключительныйДиагноз = ДанныеДиагноза.Диагноз;
				//Диагнозы травм
				ДиагнозыТравм = МКБСервер.ПолучитьДиагнозыТравм57Формы(ДанныеДиагноза.Период);
				Если ДиагнозыТравм.Найти(Объект.ЗаключительныйДиагноз) <> Неопределено Тогда
					Элементы.ВидТравмы.Видимость = Истина;
				КонецЕсли;
				Элементы.ДатаУстановления.Заголовок = "Дата установления: "
					+ Строка(ПосещенияСервер.ПолучитьДатуВремяПосещения(ДанныеДиагноза.Посещение));
			Исключение
				РезультатыПроверки.Сообщения.Добавить("Не удалось получить заключительный диагноз!");
			КонецПопытки;
				
			Если Строка(Объект.ВидСлучая) = "Случай диспансеризации" Тогда
				Элементы.РезультатОбращения.ОграничениеТипа = Новый ОписаниеТипов("СправочникСсылка.РезультатыДиспансеризации");		
				
				Объект.Исход = ИсходыЗаболеванийСервер.ПолучитьИсходДиспансеризации();
				Элементы.Исход.Доступность = Ложь;
			ИначеЕсли Строка(Объект.ВидСлучая) = "Случай поликлинический" Тогда
				Элементы.РезультатОбращения.ОграничениеТипа = Новый ОписаниеТипов("СправочникСсылка.РезультатыОбращения");
				//параметры выбора
				Элементы.РезультатОбращения.ПараметрыВыбора = РезультатыОбращенияСервер.ПолучитьПараметрыОтбораРезультатовОбращенияПоВидуСлучаяИУсловиямОказания(
						Объект.ДатаЗакрытия, Объект.ВидСлучая, Объект.УсловияОказания);
				Элементы.Исход.ПараметрыВыбора = ИсходыЗаболеванийСервер.ПолучитьПараметрыОтбораИсходовПоУсловиямОказания(
						Объект.ДатаЗакрытия, Объект.УсловияОказания);
			ИначеЕсли Строка(Объект.ВидСлучая) = "Случай стоматологический" Тогда
				Элементы.РезультатОбращения.ОграничениеТипа = Новый ОписаниеТипов("СправочникСсылка.РезультатыОбращения");	
			КонецЕсли;
		Иначе
			Для Каждого Сообщение Из РезультатыПроверки.Сообщения Цикл
				Сообщить(Сообщение);
			КонецЦикла;
		КонецЕсли;
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура ПровестиЗакрытиеСлучая(Команда)
	Если ЗначениеЗаполнено(Объект.ДатаЗакрытия) И ЗначениеЗаполнено(Объект.РезультатОбращения)
		И ЗначениеЗаполнено(Объект.Исход) И ЗначениеЗаполнено(Объект.ЗаключительныйДиагноз) Тогда
		Если Записать(Новый Структура("РежимЗаписи", РежимЗаписиДокумента.Проведение)) Тогда
			Закрыть();
		КонецЕсли;
	Иначе
		Сообщить("Необходимо заполнить сведения результата!");
	КонецЕсли;
КонецПроцедуры

&НаКлиенте
Процедура Переоткрыть(Команда)
	Если Объект.Проведен Тогда
		Попытка
			Объект.ДатаЗакрытия = Неопределено;
			Объект.РезультатОбращения = Неопределено;
			Объект.Исход = Неопределено;
			Объект.ЗаключительныйДиагноз = Неопределено;
			Объект.ГруппаЗдоровья = Неопределено;
			Элементы.Результат.Видимость = Ложь;
			Записать(Новый Структура("РежимЗаписи", РежимЗаписиДокумента.ОтменаПроведения));
			Сообщить("Случай переоткрыт!");
		Исключение
			Сообщить(ОписаниеОшибки());
			Сообщить("Переоткрытие случая не удалось" + Символы.ПС + "Обратитесь к администратору!");
		КонецПопытки;
	КонецЕсли;
КонецПроцедуры

#КонецОбласти