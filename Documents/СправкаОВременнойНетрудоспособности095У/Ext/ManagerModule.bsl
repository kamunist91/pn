﻿#Область Представление_справки

Процедура ОбработкаПолученияПолейПредставления(Поля, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	Поля.Добавить("НомерСправки");
	Поля.Добавить("ВидСправки");
	Поля.Добавить("ДатаВыдачи");
	Поля.Добавить("Пациент");
КонецПроцедуры

Процедура ОбработкаПолученияПредставления(Данные, Представление, СтандартнаяОбработка)
	СтандартнаяОбработка = Ложь;
	Представление = "Справка "
					 + "№"
					+ Данные.НомерСправки + " "
					+ Данные.ВидСправки
					+ " от "
					+ Формат(Данные.ДатаВыдачи, "ДЛФ = Д")
					+ " " + Данные.Пациент.Наименование;
КонецПроцедуры

#КонецОбласти